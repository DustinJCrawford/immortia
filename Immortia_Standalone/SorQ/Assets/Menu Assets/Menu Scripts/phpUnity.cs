﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class phpUnity : MonoBehaviour
{
    //DECLARE VARIABLES
    public InfoManager infoManager;
    public InputField usernameInput;
    public InputField passwordInput;
	public Text displayName;
	public Text Fname;
	public Text Lname;
	public Text Email;
	public Text Matches;
	public Text Wins;
	public Text Losses;
	public Text Money;
    public Text usernameLabel;
    public Text messageText;
    public InputField displayNameInput;
    public InputField firstInput;
    public InputField lastInput;
    public InputField emailInput;
    public Toggle rememberMe;
    public GameObject loginPanel;
    public GameObject signinPanel;
    public GameObject playButton;
    public GameObject editDeckButton;
    public GameObject buttonPrefab;
    public Transform deckSelectPanel;
	public Transform deckProfilePanel;
    public AudioSource clickSource;
    string formText = string.Empty; //this field is where the messages sent by PHP script will be in
    
    //HASH NEEDED TO COMMUNICATE WITH PHP FILES ON SERVER END
    private string hash = "xC6VsGdaF5RkqzT"; //change your secret code, and remember to change into the PHP file too
    
    //FIND INFOMANAGER AND CHECK IF PLAYER IS ALREADY LOGGED IN
    void Start()
    {
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        usernameInput.text = PlayerPrefs.GetString("SavedUsername", "");
        passwordInput.text = PlayerPrefs.GetString("SavedPassword", "");

        usernameInput.Select();

		if (!infoManager.getSignedIn()) 
		{
			loginPanel.SetActive(true);
		}
        else
        {
			loginPanel.SetActive (false);
			usernameLabel.text += infoManager.getDisplayName();
            StartCoroutine(getDeckIDs(infoManager.getUserID()));
        } 
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Return) && (signinPanel.activeSelf == true))
        {
            tryLogin();
        }
    }

    //METHOD CALLED WHEN SIGN-IN BUTTON IS PRESSED
    //STARTS LOGIN COROUTINE
    public void tryLogin()
    {
        string user;
        string pass;
		user = usernameInput.text.Trim();
        pass = passwordInput.text.Trim();
        StartCoroutine(Login(user, pass));
        if (rememberMe.isOn)
        {
            saveCredentials(user, pass);
        }
    }
    //METHOD TO SAVE USERNAME AND PASSWORD TO PLAYER PREFERENCES FOR EASIER SIGN IN AT A LATER DATE
    void saveCredentials(string user, string pass)
    {
        PlayerPrefs.SetString("SavedUsername", user);
        PlayerPrefs.SetString("SavedPassword", pass);
    }
    IEnumerator updateSignIn(int userid)
    {
        string url = "http://50.4.16.27:80/online.php";
        WWWForm form = new WWWForm();
        form.AddField("myform_hash", hash);
        form.AddField("myform_userID", userid);
        WWW w = new WWW(url, form);
        yield return w;
        if(w.error != null)
        {
            print(w.error);
        }
        else
        {
            print(w.text);
        }
    }
    //COROUTINE FOR SENDING USERNAME AND PASSWORD TO DATABASE FOR COMPARISON
    IEnumerator Login(string formUser, string formPassword)
    {
        string url = "http://50.4.16.27:80/check_user.php"; //change for your URL
        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_user", formUser);
        form.AddField("myform_pass", formPassword);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang
        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim(); //here we return the data our PHP told us
            string[] user = formText.Split(';');
            if (!formText.Contains("**"))
            {
                loginPanel.SetActive(false);// set panel inactive if successful login
                //SET VARIABLES IN INFOMANAGER TO BE STORED THROUGHOUT ENTIRETY OF GAMEPLAY
                infoManager.setUserID(int.Parse(user[1]));
                infoManager.setUserName(user[0]);
                infoManager.setDisplayName(user[2]);
                infoManager.setPlayerFName(user[3]);
                infoManager.setPlayerLName(user[4]);
                infoManager.setPlayerEmail(user[5]);
                infoManager.setPlayerWins(int.Parse(user[6]));
                infoManager.setPlayerLosses(int.Parse(user[7]));
                infoManager.setPlayerMoney(int.Parse(user[8]));
                infoManager.setSignedIn(true);

                usernameLabel.text = "Display Name: " + infoManager.getDisplayName();
            }
            else messageText.text = formText;
        }
        formUser = string.Empty; //just clean our variables
        formPassword = string.Empty;
        w.Dispose();
		StartCoroutine(getDeckIDs(infoManager.getUserID ()));
        StartCoroutine(updateSignIn(infoManager.getUserID()));
    }
    //COROUTINE FOR GETTING DECK IDS ASSOCIATED WITH SIGNED IN USERNAME
    IEnumerator getDeckIDs(int userID)//, Transform parent)
    {
        string url = "http://50.4.16.27:80/getDeckIDs.php";
        string[] deckIDs = new string[10];

        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_userID", userID);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang
        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text;//.Trim(); //here we return the data our PHP told us
			string[] deckStuff = formText.Split('\n');
			infoManager.setDeckInfo(deckStuff);
        }
        w.Dispose();
    }
    //POPULATE VALUES IN PROFILE PANEL INFO AND STATS
	public void populateProfileInfo()
	{
		displayNameInput.text = displayName.text = infoManager.getDisplayName();
		firstInput.text = Fname.text = infoManager.getFName();
		lastInput.text = Lname.text = infoManager.getLName();
		Email.text = infoManager.getEmail();
        Wins.text = infoManager.getWins().ToString();
		Losses.text = infoManager.getLosses().ToString();
		Matches.text = infoManager.getMatches().ToString();
		Money.text = infoManager.getMoney().ToString();
	}
    //METHOD TO START COROUTINE THAT GETS DECK IDS AND SETS THEM TO THE
    //PROPER PARENT WHICH IS DETERMINED BY WHICH BUTTON CALLED THE METHOD
    private void getDecks(Transform parent)
    {
		string[] deckStuff = infoManager.getDeckInfo ();
		for (int i = 0; i < deckStuff.Length - 1; i++)
		{
			string[] temp = deckStuff[i].Split(';');
			GameObject button = (GameObject)Instantiate(buttonPrefab);
			button.GetComponentInChildren<Text>().text = temp[1];
			button.GetComponent<Button>().onClick.AddListener(
				() =>
				{
				infoManager.setDeckID(int.Parse(temp[0]));
				playButton.SetActive(true);
				editDeckButton.SetActive(true);
				clickSource.Play();
			});
			button.transform.SetParent(parent, false);
		}
    }
    //ATTACHED TO PROFILE BUTTON TO CREATE LIST AND SET PARENT TO PROFILE PANEL
	public void profileDecks()
	{
		getDecks (deckProfilePanel);
	}
    //ATTACHED TO PLAY BUTTON TO CREATE LIST AND SET PARENT TO DECK SELECTION
	public void playDecks()
	{
		getDecks (deckSelectPanel);
	}
    //METHOD TO RESET DECK PANELS
	public void initializeComponents()
	{
		//empty current list so as to not relist the same lobbies more than once
		List<GameObject> children = new List<GameObject>();
		foreach (Transform child in deckSelectPanel) children.Add(child.gameObject);
		if (children != null)
        {
            children.ForEach (child => Destroy (child));
        }
		//empty current list so as to not relist the same lobbies more than once
		List<GameObject> children1 = new List<GameObject>();
		foreach (Transform child in deckProfilePanel) children1.Add(child.gameObject);
		if (children1 != null)
		{
			children1.ForEach (child => Destroy (child));
		}
	}

    //SIGN OUT
    //REPOPULATE INPUT FIELDS WITH SAVED INFORMATION
    //RESET PLAYER INFORMATION
    public void logout()
    {
        usernameInput.text = PlayerPrefs.GetString("SavedUsername");
        passwordInput.text = PlayerPrefs.GetString("SavedPassword");
        infoManager.setSignedIn(false);
		initializeComponents ();
		loginPanel.SetActive(true);
		usernameLabel.text = "Username: ";
		messageText.text = string.Empty;
        StartCoroutine(infoManager.updateSignOut(infoManager.getUserID()));
    }
}