﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundManager : MonoBehaviour {
    public AudioSource soundSource;
    public AudioSource musicSource;
    public AudioSource clickSource;
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider soundSlider;
    public Text masterText;
    public Text musicText;
    public Text soundText;
    private float masterVolume;
    private float musicStartVolume;
    private float soundStartVolume;


	// Use this for initialization
	void Start ()
    {
        musicSource = GameObject.Find("musicSource").GetComponent<AudioSource>();
        soundSource = GameObject.Find("soundSource").GetComponent<AudioSource>();
        this.masterVolume = PlayerPrefs.GetFloat("Master Volume");
        this.musicStartVolume = PlayerPrefs.GetFloat("Music Volume");
        this.soundStartVolume = PlayerPrefs.GetFloat("Sound Volume");
        resetVolumeSettings();
	}

    //update new volumes on slider change
    void Update()
    {
        masterText.text = masterSlider.value.ToString();
        musicSource.volume = (masterSlider.value / 100) * (musicSlider.value / 100);
        musicText.text = musicSlider.value.ToString();
        soundSource.volume = (masterSlider.value / 100) * (soundSlider.value / 100);
        soundText.text = soundSlider.value.ToString();
        clickSource.volume = soundSource.volume;
    }

    //When back button is pushed, cancel volume changes
    public void resetVolumeSettings()
    {
        masterSlider.value = masterVolume * 100;
        musicSlider.value = musicStartVolume * 100;
        musicSource.volume = musicStartVolume;
        soundSlider.value = soundStartVolume * 100;
        soundSource.volume = soundStartVolume;
        clickSource.volume = soundSource.volume;
    }

    public void updatePlayerPrefs()
    {
        PlayerPrefs.SetFloat("Master Volume", masterSlider.value / 100);
        PlayerPrefs.SetFloat("Music Volume", musicSlider.value / 100);
        PlayerPrefs.SetFloat("Sound Volume", soundSlider.value / 100);
        this.masterVolume = PlayerPrefs.GetFloat("Master Volume");
        this.musicStartVolume = PlayerPrefs.GetFloat("Music Volume");
        this.soundStartVolume = PlayerPrefs.GetFloat("Sound Volume");
    }
}
