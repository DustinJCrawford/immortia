﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour
{

    #region Members

    public StateMachine.State gameState = new StateMachine.State();
    public AttackManager attackManager;
    public SoundManager soundManager;
    public InfoManager infoManager;
	public EffectManager effectManager;
	public Board gameBoard = new Board();
	public TriggerSystem trigSystem = new TriggerSystem();

	public Material cardBack;
    public Material cardBack2;
    public GameObject cardPrefab;
    public GameObject cardSpellPrefab;
    public GameObject spellPrefab;
    private GameObject spell;
    private GameObject card;
    public GameObject playerSpellZone;
    public GameObject enemySpellZone;
    public GameObject spellZoneIcon;
    public Animation startTurnAnim;
    public Text playAreaFullText;
    public BoxCollider2D enemyHandZone;
    public Slider playerHealthSlider;
    public Slider enemyHealthSlider;
    public Outline playerOutline;
    public Outline enemyOutline;
	public GameObject errorPanel;
    public GameObject settingsPanel;

    public GameObject spellInfoPanel;
    
	public float turnTimer = 60;
    public float countdownTime;
    public bool isPlayerTurn;
    public bool animPlayed;
    public bool shieldWall;
    public bool enemyShieldWall;
    public bool fieldUpdated;
    public bool gameOver;
    public bool selectCard;
    public int activeSpellCardID;
    private bool startTurn;
    public Vector3 playerToken;
    public Vector3 enemyToken;

    public Player player = new Player();
    public string playerName;
    public Player enemyPlayer = new Player();

    public bool pauseScreenShown;
    public bool masterCardSelect;
    public bool gameSceneLoaded;
	public bool panelInPlace = false;

    public bool destroyActiveSpell; //This is for destroying triggers and potentially other spells.
    public bool destroySpell; //This is the Destroy Card method
    public bool equipReady;
    public bool destroyingCard;
	
    public bool playerInitialized; // Part of stopping instant card form going without deck
    public bool deckInitialized; // Stop instant card from being performed until the deck is returned from the database.

    delegate void delayedMethod();

    ShowDatabase sdb = new ShowDatabase();

    public Text gameResultsText;
    public GameObject gameResultsPanel;
    #endregion

    // Use this for initialization
	void Start () 
	{
        attackManager = GameObject.Find("AttackManager").GetComponent<AttackManager>();
        soundManager = GameObject.Find("SettingsManager").GetComponent<SoundManager>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        effectManager = GameObject.Find("EffectManager").GetComponent<EffectManager>();
        //settingsPanel = GameObject.Find("SettingsPanel").gameObject;
        //settingsPanel.SetActive(false);
        //pauseScreenShown = false;
        sdb = gameObject.AddComponent("ShowDatabase") as ShowDatabase;
        infoManager.initializeGameStats();
        gameState = StateMachine.State.Idle;
	}
	
	// Update is called once per frame
	void Update () 
	{

        // Boolean that will start the game after deck is initialized in deck
	    if (!deckInitialized && playerInitialized)
	    {
            gameResultsText = GameObject.Find("gameResultsText").GetComponent<Text>();
	        Debug.Log("Check if deck is initialized");
	        if (player.playerDeck.initialized)
	        {
	            Debug.Log("Deck Initialized = true");
	            deckInitialized = true;
	            player.playerDeck.shuffle();
				//player.playerDeck.addSpecialCard();
	            Debug.Log("Shuffled Deck");
	            drawCard(5);
                //StartCoroutine(sdb.loadTable("loadAllCards.php"));
	        }
	    }

		if (panelInPlace && gameSceneLoaded) 
		{
			StartCoroutine(moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, -835.0f, 0.0f), 0.25f));
			panelInPlace = false;
		}

        // If the boolean value fieldUpdated is set to true (this occurs when any cards are destroyed), then the scanForShieldWall method gets called.
        // scanForShieldWall moves through all cards on the field that are in play, and if any of them have shield wall, the game sets your shield wall to true,
        // and uses an RPC method to send your shield wall boolean to the opposing player. fieldUpdated is then set to false at the end of scanForShieldWall.

        if (fieldUpdated)
        {
            //Put another (yet to be written) method here that will re-arrange cards on the field as necessary.
            // Something like:
            // reorderCards(); 
            // Will use new moveCard() method to "animate" the movement of each card.

            scanForShieldWall();
        }
        
        // If the player's health reaches zero, the game is over and the player is given a loss and the infoManager is updated with the new loss value.
        
        if (player.playerHealth <= 0  && !gameOver)
        {
            //gameResultsText = GameObject.Find("gameResultsText").GetComponent<Text>();
            //gameResultsPanel = GameObject.Find("gameResultsPanel");
            //gameResultsPanel.SetActive(true);
            int lose = infoManager.getLosses();
            gameOver = true;
			lose++;
			infoManager.setPlayerLosses(lose);
            gameResultsText.text = "YOU LOSE!";
            Debug.Log("YOU LOSE");
        }

        // If the enemy player's health reaches zero, the game is over and the player is given a win and the infoManager is updated with the new win value.

        if (enemyPlayer.playerHealth <= 0 && !gameOver)
        {
            //gameResultsText = GameObject.Find("gameResultsText").GetComponent<Text>();
            //gameResultsPanel = GameObject.Find("gameResultsPanel");
            //gameResultsPanel.SetActive(true);
            int win = infoManager.getWins();
            gameOver = true;
			win++;
			infoManager.setPlayerWins(win);
            gameResultsText.text = "YOU WIN!";
            Debug.Log("YOU WIN");
            StartCoroutine(loadMenuEndGame(10.0f));
        }


        // This is in Update, but is only called once, when the game scene is first loaded.
        // The local player's first turn boolean is set to true, and the hand and play positions 
        // are created. Player and enemy health sliders are found for use later when incrementing/decrementing
        // health values. Then the player info is loaded and the turn timer is started.
        // The game then determines which player will go first, and finally the gameSceneLoaded boolean is set to 
        // true so this code block will not be called again.

        if (Application.loadedLevel.Equals(2) && !gameSceneLoaded)
        {
			errorPanel = GameObject.Find ("errorButton");
			player.isFirstTurn = true;
			gameBoard.createPlayPositions();
			gameBoard.createHandPositions();
            playerHealthSlider = GameObject.Find("playerHealthSlider").GetComponent<Slider>();
            enemyHealthSlider = GameObject.Find("enemyHealthSlider").GetComponent<Slider>();
            settingsPanel = GameObject.Find("settingsPanel").gameObject;
            settingsPanel.SetActive(false);
            pauseScreenShown = false;

            if (!playerOutline)
            {
                playerOutline = GameObject.Find("playerInfoPanel").GetComponent<Outline>();
            }

            if (!enemyOutline)
            {
                enemyOutline = GameObject.Find("enemyPlayerInfoPanel").GetComponent<Outline>();
            }
            spellInfoPanel = GameObject.Find("spellInfoPanel");

            loadPlayer();
            countdownTime = turnTimer;
            determineFirstMove();
            spellInfoPanel.SetActive(false);
            gameOver = false;
            startTurn = true;
            gameSceneLoaded = true;
        }

        // This code block is called at the start of the player's turn, and clears the daze attribute from any
        // cards that currently have it, and also clears the previous turn's played spells. A single card is then
        // drawn and given to the player and their power is reset and incremented.
        if (player.isTurn && startTurn && deckInitialized)
        {
            StartCoroutine(ShowMessage("YOUR TURN!", 2.0f));
            isPlayerTurn = true;
            undazeCards();
            effectManager.clearSpells();
            drawCard(1);
            resetPlayerPower();
            playerOutline.enabled = true;
            enemyOutline.enabled = false;
        }
            else if (!player.isTurn && gameSceneLoaded && Application.loadedLevel.Equals(2))
            {
                playerOutline.enabled = false;
                enemyOutline.enabled = true;
            }


        // Code block that decrements the turn timer, and once it reaches zero, ends the player's turn automatically.

        if (countdownTime > 0 && isPlayerTurn)
        {
            
            countdownTime -= Time.deltaTime;

            if (countdownTime <= 0)
            {
                endTurn();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape) && Application.loadedLevel.Equals(2))
        {
            if (pauseScreenShown)
            {
                Debug.Log("Pause screen hidden");
                settingsPanel.SetActive(false);
                pauseScreenShown = false;
            }

            else if (!pauseScreenShown)
            {
                Debug.Log("Pause screen shown");
                settingsPanel.SetActive(true);
                pauseScreenShown = true;
            }

        }   
	    
	}

    IEnumerator ShowMessage(string message, float delay)
    {
        gameResultsText.text = message;
        yield return new WaitForSeconds(delay);
        gameResultsText.text = string.Empty;
    }

    void OnDestroy()
    {
        gameSceneLoaded = false;
    }

    IEnumerator loadMenuEndGame(float delay)
    {
        yield return new WaitForSeconds(delay);
        goToPostGameLobby();
        networkView.RPC("destroyGameManager", RPCMode.AllBuffered);
    }

    // Method that determines which player will go first. This is called by both clients. However,
    // given the nature of the code block's logic, only the client who is currently the "host" server
    // will perform the calculation and update the turns accordingly.

    void determineFirstMove()
    {
        if (Network.isServer)
        {
            int randomNum;

            randomNum = Random.Range(1, 100);

            if (randomNum <= 49)
            {
                isPlayerTurn = true;
                player.isTurn = true;
                enemyPlayer.isTurn = false;
                networkView.RPC("getTurn", RPCMode.Others, enemyPlayer.isTurn);
            }

            else if (randomNum >= 50)
            {
                isPlayerTurn = false;
                player.isTurn = false;
                enemyPlayer.isTurn = true;
                networkView.RPC("getTurn", RPCMode.Others, enemyPlayer.isTurn);
            }
        }
    }

    // Method that ends a player's turn. An array of GameObjects named "removeEnemySpells" is created and filled with all objects of tag "OpponentSpell".
    // removeEnemySpells is then iterated through, and if the spell's type is either trigger or continuous, the spell is not destroyed. If it is any 
    // other spell type, it is destroyed.

    // All card attack booleans are then reset and the player's turn boolean is set to false. RPC calls are made to swap the turn for the players,
    // and the turn timer is reset.

    public void endTurn()
    {
        GameObject[] removeEnemySpells = GameObject.FindGameObjectsWithTag("OpponentSpell");
        for (int z = 0; z < removeEnemySpells.Length; z++)
        {
            if (removeEnemySpells[z].GetComponent<spellIconController>().srcCard.cardType == Card.cardTypes.spellTrig || removeEnemySpells[z].GetComponent<spellIconController>().srcCard.cardType == Card.cardTypes.spellCont)
            {
                continue;
            }
                 
            Destroy(removeEnemySpells[z]);
            gameBoard.enemySpellPosAvail[z] = true;
        }

        globalCardAttackReset();
        player.isTurn = false;
        isPlayerTurn = false;
        enemyPlayer.isTurn = true;
        playerOutline.enabled = false;
        enemyOutline.enabled = true;
        networkView.RPC("swapTurn", RPCMode.Others, enemyPlayer.isTurn);
        networkView.RPC("scanForShieldWall", RPCMode.All);
        countdownTime = turnTimer;
        startTurn = true;
    }

    // Method that populates an array of Card type and iterates through it, scanning for any cards that are dazed and decrementing their daze counter.

    void undazeCards()
    {
        Card[] cardsInPlay;

        cardsInPlay = GameObject.FindObjectsOfType<Card>();

        for (int i = 0; i < cardsInPlay.Length; i++)
        {
            if (cardsInPlay[i].gameObject.tag == "Player" && cardsInPlay[i].isDazed  && cardsInPlay[i].GetComponent<CardController>().cardInPlay)
            {
                decrementDaze(cardsInPlay[i].networkView.viewID);
            }
        }

        startTurn = false;
    }
	

    public void drawCard(int drawAmount)
    {

        for (int i = 0; i < drawAmount; i++)
        {

            NetworkViewID viewID = Network.AllocateViewID();

            if (!gameBoard.isHandSpaceAvailable())
            {
                showError(2);
            }

            if (gameBoard.isHandSpaceAvailable())
            {
                createCard(viewID);
                Debug.Log("Sending function call to other clients.");
                networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);

            }
        }

    }

    // Method that compares a card's power amount to the player's current power level.
    // Returns true if the card's power level is less than or equal to the player's
    // current power level. Also decreases the player's power by the card's power amount.

    public bool canPlayCard(GameObject card, int cardPower)
    {

        if (cardPower <= player.playerCurrentPower)
        {
            return true;
        }

        else return false;

    }


    public void showError(int errorCode)
    {
        if (errorCode == 1)
        {
            Debug.Log("Test");
        }

        if (errorCode == 2)
        {
            Debug.Log("No hand space available");
			errorPanel.gameObject.GetComponentInChildren<Text>().text = "NO HAND SPACE AVAILABLE";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
            

        }

        if (errorCode == 3)
        {
            Debug.Log("It isn't your turn!");
			errorPanel.GetComponentInChildren<Text>().text = "IT ISN'T YOUR TURN!";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));


        }

        if (errorCode == 4)
        {
            Debug.Log("Not enough power!");
			errorPanel.GetComponentInChildren<Text>().text = "NOT ENOUGH POWER!";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
            ;

        }

        if (errorCode == 5)
        {
            Debug.Log("Card has already attacked!");
			errorPanel.GetComponentInChildren<Text>().text = "CARD HAS ALREADY ATTACKED THIS TURN!";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));

        }

        if (errorCode == 6)
        {
            Debug.Log("Invalid target!");
			errorPanel.GetComponentInChildren<Text>().text = "INVALID TARGET!";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
        }

        if (errorCode == 7)
        {
            Debug.Log("Cannot attack with that card. It is dazed!");
			errorPanel.GetComponentInChildren<Text>().text = "CARD IS DAZED!";
            StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
        }

		if (errorCode == 8)
		{
			Debug.Log("Not enough space in hand!");
			errorPanel.GetComponentInChildren<Text>().text = "NOT ENOUGH HAND SPACE!";
			StartCoroutine(waitForPanel(2.0f));
			StartCoroutine (moveObject (errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
		}

        if (errorCode == 9)
        {
            Debug.Log("Not enough equip slots!");
            errorPanel.GetComponentInChildren<Text>().text = "INVALID EQUIP TARGET!";
            StartCoroutine(waitForPanel(2.0f));
            StartCoroutine(moveObject(errorPanel, errorPanel.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f), 0.25f));
        }

    }

    public void loadPlayer()
    {
        player = new Player();
        player.playerDeck = new Deck();
        playerInitialized = true;
        populatePlayerDeck();
        enemyPlayer = new Player();
        player.playerUserName = "testName";
        player.playerUserID = infoManager.getUserID();
        player.playerDisplayName = infoManager.getDisplayName();
        playerName = player.playerDisplayName;
		player.playerCurrentPower = 0;
        player.playerNextTurnPower = 1;
        GameObject.Find("playerNameText").GetComponent<Text>().text = player.playerDisplayName;
        Debug.Log(player.playerHealth.ToString());
        setPlayerHPText();
        setPlayerCurrentPower();
        setPlayerTotalPower();
        networkView.RPC("loadPlayerForEnemy", RPCMode.Others, player.playerDisplayName);
        networkView.RPC("updateHealthForEnemy", RPCMode.Others, player.playerHealth);
        networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);
    }

    public void populatePlayerDeck()
    {
        StartCoroutine(player.playerDeck.getDeck(infoManager.getDeckID()));
    }

    //Instantiating card for local client. Information will be pulled from the database in this method, or another method will be called to pull 
    //database information within this method.
    //Method also places card in an available hand position.
    void createCard(NetworkViewID viewID)
    {
        Card newCard = player.playerDeck.getCard();
        int cardID = newCard.cardID;
        int power = newCard.cardPwr;
        int strength = newCard.cardStr;
        int vitality = newCard.cardVit;
        int equip = newCard.cardEqp;
        bool shield = newCard.hasShieldWall;
        bool qw = newCard.hasQuickWitted;
        bool pierce = newCard.hasPiercing;
        string name = newCard.cardName;
        Sprite sprite = newCard.cardSprite;
        bool isSpell = newCard.isSpell;
        string desc = newCard.cardDesc;
        string type = newCard.type;
        string attr = newCard.attr;
        Sprite typeSprite = newCard.typeSprite;

        if (isSpell)
        {
            card = (GameObject) Instantiate(cardSpellPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
            
        }
        else if (!isSpell)
        {
            card = (GameObject)Instantiate(cardPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
            
        }

        addClassComponent(cardID);

        card.networkView.viewID = viewID;
        card.transform.Rotate(270.0f, 0.0f, 0.0f, Space.World);
        StartCoroutine(moveCard(viewID, card.transform.position, gameBoard.calcHandPosition(), 0.5f));
        card.GetComponent<Card>().cardName = name;
        card.GetComponent<Card>().cardPwr = power;
        card.GetComponent<Card>().cardStr = strength;
        card.GetComponent<Card>().cardVit = vitality;
        card.GetComponent<Card>().cardEqp = equip;
        card.GetComponent<Card>().hasShieldWall = shield;
        card.GetComponent<Card>().hasQuickWitted = qw;
        card.GetComponent<Card>().hasPiercing = pierce;
        card.GetComponent<Card>().cardOrigEqp = equip;
        card.GetComponent<Card>().cardOrigPwr = power;
        card.GetComponent<Card>().cardOrigStr = strength;
        card.GetComponent<Card>().cardOrigVit = vitality;
        card.GetComponent<Card>().cardSprite = sprite;

        card.GetComponent<Card>().isSpell = isSpell;
        card.GetComponent<Card>().cardDesc = desc;
        card.GetComponent<Card>().type = type;
        card.GetComponent<Card>().cardType = newCard.cardType;
        card.GetComponent<Card>().attr = attr;

        bool cardDazed = false;
        if (!qw)
        {
            card.GetComponent<Card>().dazeDuration = 1;
            card.GetComponent<Card>().isDazed = true;
            cardDazed = true;
        }
        else if(qw)
        {
            card.GetComponent<Card>().dazeDuration = 0;
            card.GetComponent<Card>().isDazed = false;
        }

		gameBoard.handSpaceAvailable--;
        card.tag = "Player";
        updateCardStats(viewID, card.GetComponent<Card>().cardName, cardID, power, strength, vitality, equip, shield, qw, pierce, cardDazed, sprite, isSpell, desc, type);
        gameObject.networkView.RPC("createCardForEnemy", RPCMode.OthersBuffered, viewID, name, cardID, power, strength, vitality, equip, shield, qw, pierce, cardDazed, isSpell, desc, type);

    }


    void addClassComponent(int cardID)
    {

        switch (cardID)
        {
			case 32: card.AddComponent<HolyArrow>();
				break;
			case 31: card.AddComponent<GiftOfTheSaints>();
				break;
            case 33: card.AddComponent<RighteousAxe>();
                break;
			case 41: card.AddComponent<BlessedArmory>();
				break;
            case 46: card.AddComponent<LeechLife>();
                break;
            case 81: card.AddComponent<Insight>();
                break;
			case 88: card.AddComponent<BlackHole>();
				break;
			case 116: card.AddComponent<HealingWaters>();
				break;
			case 119: card.AddComponent<LavaStream>();
				break;
			case 120: card.AddComponent<TidalWave>();
				break;
            case 136: card.AddComponent<SuitUp>();
                break;
            case 139: card.AddComponent<ArchDig>();
                break;

            default:
                break;
        }

    }


    //Displays the stats on a card's canvas with a given NetworkViewID. 
    void updateCardStats(NetworkViewID viewID, string name, int cardID, int pwr, int str, int vit, int eqp, bool shield, bool qw, bool pierce, bool daze, Sprite sprite, bool isSpell, string desc, string type)
    {
        Component[] compArray;
        Image cardImage;

        compArray = NetworkView.Find(viewID).GetComponentsInChildren<Text>();
        cardImage = NetworkView.Find(viewID).GetComponentInChildren<Image>();

        NetworkView.Find(viewID).gameObject.GetComponent<Card>().hasShieldWall = shield;
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().hasPiercing = pierce;
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().hasQuickWitted = qw;
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().cardID = cardID;
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().isSpell = isSpell;
        cardImage.sprite = sprite;

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compArray[i].name == "nameText")
            {
                compArray[i].GetComponent<Text>().text = name;
            }

            if (compArray[i].name == "powerText")
            {
                compArray[i].GetComponent<Text>().text = pwr.ToString();
            }

            if (compArray[i].name == "strengthText")
            {
                compArray[i].GetComponent<Text>().text = str.ToString();
            }

            if (compArray[i].name == "typeText")
            {
                compArray[i].GetComponent<Text>().text = translateType(type);
            }

            if (compArray[i].name == "vitalityText")
            {
                if (!isSpell) compArray[i].GetComponent<Text>().text = vit.ToString();
                else compArray[i].GetComponent<Text>().text = "";
            }

            if (compArray[i].name == "equipText")
            {
                if (!isSpell) compArray[i].GetComponent<Text>().text = eqp.ToString();
                else compArray[i].GetComponent<Text>().text = "";
            }

            if (compArray[i].name == "descriptionText")
            {
                compArray[i].GetComponent<Text>().text = desc;
                if(shield)compArray[i].GetComponent<Text>().text += "\n-Shield Wall";
                if (qw) compArray[i].GetComponent<Text>().text += "\n-Quick-Witted";
                if (pierce) compArray[i].GetComponent<Text>().text += "\n-Piercing";
            }

        }
    }

    string translateType(string card)
    {
        string returnString;
        if (card == "SPELL-N")
        {
            returnString = "Normal";
        }
		else if (card == "SPELL-E")
        {
            returnString = "Equip";
        }
		else if (card == "SPELL-T")
        {
            returnString = "Trigger";
        }
        else if (card == "SPELL-C")
        {
            returnString = "Continuous";
        }
        else 
		{ 
			returnString = "ERROR"; 
		}

        return returnString;
    }

    public void updateMinionCardVit(NetworkViewID viewID, int vit)
    {
        Component[] compArray;
        compArray = NetworkView.Find(viewID).GetComponentsInChildren<Text>();

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compArray[i].name == "vitalityText")
            {
                compArray[i].GetComponent<Text>().text = vit.ToString();
            }
        }

    }

    void setPlayerHPText()
    {
        GameObject.Find("playerHealthValue").GetComponent<Text>().text = player.playerHealth.ToString();
        networkView.RPC("updateHealthForEnemy", RPCMode.Others, player.playerHealth);
    }

    public void minusPlayerHP(int amount)
    {
        player.playerHealth -= amount;
        GameObject.Find("playerHealthValue").GetComponent<Text>().text = player.playerHealth.ToString();
        networkView.RPC("updateHealthForEnemy", RPCMode.Others, player.playerHealth);
    }

    public void minusEnemyPlayerHP(int amount)
    {
        enemyPlayer.playerHealth -= amount;
        GameObject.Find("enemyHealthValue").GetComponent<Text>().text = enemyPlayer.playerHealth.ToString();

    }

    public void addPlayerHP(int amount)
    {
        player.playerHealth += amount;
        GameObject.Find("playerHealthValue").GetComponent<Text>().text = player.playerHealth.ToString();
        networkView.RPC("updateHealthForEnemy", RPCMode.OthersBuffered, player.playerHealth);
    }

    public void resetPlayerPower()
    {
        player.playerCurrentPower = player.playerNextTurnPower;

        if (player.playerCurrentPower < player.playerMaxPower)
        {
            player.playerNextTurnPower++;
        }
        
        GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = player.playerCurrentPower.ToString();
        networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);
    }

    void setPlayerCurrentPower()
    {
        GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = player.playerCurrentPower.ToString();
    }

    void setPlayerTotalPower()
    {
        GameObject.Find("playerPowerTotal").GetComponent<Text>().text = player.playerMaxPower.ToString();
        networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);
    }

    public void minusPlayerPower(int amount)
    {
        player.playerCurrentPower -= amount;
        GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = player.playerCurrentPower.ToString();
        networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);
    }

    public void addPlayerPower(int amount)
    {
        if (player.playerCurrentPower < player.playerMaxPower)
        {
            player.playerCurrentPower += amount;
            GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = player.playerCurrentPower.ToString();
            networkView.RPC("updatePowerForEnemy", RPCMode.Others, player.playerCurrentPower, player.playerMaxPower);
        }
    }

    public void startGameFromLobby()
    {
        networkView.RPC("startGame", RPCMode.All);
    }

	public void goToPostGameLobby()
	{
		networkView.RPC("postGameLobby", RPCMode.All);
		infoManager.setPostGame (true);
	}

    public void globalCardAttackReset()
    {
        GameObject[] cardArray;

        cardArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < cardArray.Length; i++)
        {
            cardArray[i].GetComponent<CardController>().hasAttacked = false;
            cardArray[i].GetComponent<CardController>().cardSelected = false;
            cardArray[i].transform.localScale = cardArray[i].GetComponent<CardController>().origScale;
            cardArray[i].GetComponent<CardController>().clickCount = 0;
        }
    }

    public void globalCardInPlayDestroy()
    {
        Card[] cards;
        GameObject[] cardObjects;

        cards = GameObject.FindObjectsOfType<Card>();
        cardObjects = new GameObject[cards.Length];

        for (int i = 0; i < cards.Length; i++)
        {
            cardObjects[i] = cards[i].gameObject;
        }

        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i].GetComponent<CardController>().cardInPlay)
            {
                cards[i].GetComponent<Card>().cardVit = 0;
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, cards[i].networkView.viewID, cards[i].GetComponent<Card>().cardVit, "vitalityText");
                StartCoroutine(checkToDestroy(cards[i].networkView.viewID));
                //networkView.RPC("destroyCard", RPCMode.All, (cardObjects[i].networkView.viewID));
            }
        }
    }

    public IEnumerator checkToDestroy(NetworkViewID viewID)
    {
		//Vector3 newPosition = NetworkView.Find (viewID).transform.position;
        Vector3 destroyPos = new Vector3(-1000.0f, 0.0f, 0.0f);

        Debug.Log("Within checkToDestroy");

        if (NetworkView.Find(viewID).GetComponent<Card>().cardVit <= 0)
        {
            Debug.Log("Within checkToDestroy if-statement");
            Debug.Log(NetworkView.Find(viewID).GetComponent<CardController>().origPos.ToString());
            StartCoroutine(moveCard(viewID, NetworkView.Find (viewID).GetComponent<CardController>().origPos, destroyPos, 0.5f));
            networkView.RPC("moveCardForEnemy", RPCMode.Others, viewID);
            yield return new WaitForSeconds(1.0f);
            networkView.RPC("destroyCard", RPCMode.All, viewID);
        }
    }


	public void healFriendlyMinion(NetworkViewID viewID, int amount)
	{

	}


	public void healAllFriendlyMinions(int amount)
    {
        GameObject[] cardArray;
        NetworkViewID viewID;
        cardArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < cardArray.Length; i++)
        {

            viewID = cardArray[i].gameObject.networkView.viewID;

            if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard)
            {
                cardArray[i].GetComponent<Card>().cardVit += amount;

				if (cardArray[i].GetComponent<Card>().cardVit > cardArray[i].GetComponent<Card>().cardOrigVit)
				{
					cardArray[i].GetComponent<Card>().cardVit = cardArray[i].GetComponent<Card>().cardOrigVit;
				}

                updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");
                
            }
        }

    }

	public void healAllFriendlyMinionsInPlay(int amount)
	{
		GameObject[] cardArray;
		NetworkViewID viewID;
		cardArray = GameObject.FindGameObjectsWithTag("Player");
		
		for (int i = 0; i < cardArray.Length; i++)
		{
			
			viewID = cardArray[i].gameObject.networkView.viewID;
			
			if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard && cardArray[i].GetComponent<CardController>().cardInPlay)
			{

				if (cardArray[i].GetComponent<Card>().cardVit < cardArray[i].GetComponent<Card>().cardOrigVit)
				{
					cardArray[i].GetComponent<Card>().cardVit += amount;

					if (cardArray[i].GetComponent<Card>().cardVit > cardArray[i].GetComponent<Card>().cardOrigVit)
					{
						cardArray[i].GetComponent<Card>().cardVit = cardArray[i].GetComponent<Card>().cardOrigVit;
					}

				}

				updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");
				
			}
		}
		
	}
	
    public void damageAllFriendlyMinions(int amount)
    {
        GameObject[] cardArray;
        NetworkViewID viewID;
        cardArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < cardArray.Length; i++)
        {

            viewID = cardArray[i].gameObject.networkView.viewID;

            if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard && cardArray[i].gameObject.networkView.viewID == viewID)
            {
                Debug.Log(cardArray[i].GetComponent<Card>().cardVit.ToString() + " - " + amount.ToString());
                cardArray[i].GetComponent<Card>().cardVit -= amount;
                updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");

                if (cardArray[i].GetComponent<Card>().cardVit <= 0)
                {
                    networkView.RPC("destroyCard", RPCMode.All, viewID);
                }

            }
        }
    }
	
	public void damageAllEnemyMinions(int amount) 
	{
		GameObject[] cardArray;
		NetworkViewID viewID;
		
		cardArray = GameObject.FindGameObjectsWithTag ("Opponent");
		
		for (int i = 0; i < cardArray.Length; i++) 
		{
			
			viewID = cardArray[i].gameObject.networkView.viewID;
			
			if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard) 
			{
				cardArray[i].GetComponent<Card>().cardVit -= amount;
				updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");
				
				if (cardArray[i].GetComponent<Card>().cardVit <= 0) 
				{
					networkView.RPC ("destroyCard", RPCMode.All, cardArray[i].gameObject.networkView.viewID);
				}
				
			}
		}
	}


	public void damageAllEnemyMinionsInPlay(int amount) 
	{
		GameObject[] cardArray;
		NetworkViewID viewID;
		
		cardArray = GameObject.FindGameObjectsWithTag ("Opponent");
		
		for (int i = 0; i < cardArray.Length; i++) 
		{
			
			viewID = cardArray[i].gameObject.networkView.viewID;
			
			if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard && cardArray[i].GetComponent<CardController>().cardInPlay) 
			{
				cardArray[i].GetComponent<Card>().cardVit -= amount;
				updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");
				
				if (cardArray[i].GetComponent<Card>().cardVit <= 0) 
				{
					StartCoroutine(checkToDestroy (cardArray[i].networkView.viewID));
					//networkView.RPC ("destroyCard", RPCMode.All, cardArray[i].gameObject.networkView.viewID);
				}
				
			}
		}
	}

	
    public void increaseAllFriendlyMinionStr(int amount)
    {
        GameObject[] cardArray;

        cardArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < cardArray.Length; i++)
        {
            if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard)
            {
                cardArray[i].GetComponent<Card>().cardStr += amount;
            }
        }
    }

	public void increaseAllFriendlyMinionVit(int amount)
	{
		GameObject[] cardArray;
		NetworkViewID viewID;
		cardArray = GameObject.FindGameObjectsWithTag("Player");
		
		for (int i = 0; i < cardArray.Length; i++)
		{
			
			viewID = cardArray[i].gameObject.networkView.viewID;
			
			if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard)
			{
				cardArray[i].GetComponent<Card>().cardVit += amount;
				updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
				networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");
				
			}
		}
	}

    public void increaseAllFriendlyMinionVitInPlay(int amount)
    {
        GameObject[] cardArray;
        NetworkViewID viewID;
        cardArray = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < cardArray.Length; i++)
        {

            viewID = cardArray[i].gameObject.networkView.viewID;

            if (cardArray[i].GetComponent<Card>().cardType == Card.cardTypes.minionCard && cardArray[i].GetComponent<CardController>().cardInPlay)
            {
                cardArray[i].GetComponent<Card>().cardVit += amount;
                updateMinionCardVit(viewID, cardArray[i].GetComponent<Card>().cardVit);
                networkView.RPC("updateCardVitForEnemy", RPCMode.Others, viewID, cardArray[i].GetComponent<Card>().cardVit, "vitalityText");

            }
        }
    }

    public void stopDaze(NetworkViewID viewID)
    {
        Component[] components;
        Text dazeText;
        Card card = NetworkView.Find(viewID).GetComponent<Card>();
        card.isDazed = false;
        Debug.Log("Undazed "+card.cardName);
        components = card.GetComponentsInChildren<Text>();

        for (int i = 0; i < components.Length; i++)
            {
            if (components[i].name == "dazedText")
            {
                dazeText = (Text)components[i];
                dazeText.enabled = false;
            }
        }
    }

    public void decrementDaze(NetworkViewID viewID)
    {
        Card card = NetworkView.Find(viewID).GetComponent<Card>();
        card.dazeDuration--;

        if (card.dazeDuration <= 0)
        {
            stopDaze(viewID);
        }
    }


    #region RPCMethods
    //RPC Methods. Methods that can be called remotely from one client to execute on another client's machine through the network.


    [RPC]
    public void moveCardForEnemy(NetworkViewID viewID)
    {
		Vector3 newPosition = NetworkView.Find (viewID).transform.position;
        Vector3 destPos = new Vector3(-1000.0f, 0.0f, 0.0f);
        //StartCoroutine(moveCard(viewID, NetworkView.Find(viewID).GetComponent<CardController>().origPos, destPos, 0.5f));
		StartCoroutine(moveCard(viewID, newPosition, destPos, 0.5f));
    }

	[RPC]
	public void callAttackCardMoveForEnemy(NetworkViewID attackViewID, NetworkViewID defendViewID, float time) 
	{
		StartCoroutine (attackCardMoveForEnemy(attackViewID, defendViewID, time));
	}


    
    public IEnumerator attackCardMoveForEnemy(NetworkViewID attackViewID, NetworkViewID defendViewID, float time)
    {
		Vector3 origPos = NetworkView.Find(attackViewID).gameObject.transform.position;
        Vector3 destPos = NetworkView.Find(defendViewID).gameObject.transform.position;
		destPos = new Vector3 (destPos.x, destPos.y + 50, destPos.z);

        StartCoroutine(moveCard(attackViewID, origPos, destPos, time));
        yield return new WaitForSeconds(time);


        StartCoroutine(moveCard(attackViewID, destPos, origPos, time));

    }


    [RPC] public void scanForShieldWall()
    {
        bool inPlay;
        bool hasShield;

        Card[] cardsOnField;

        cardsOnField = GameObject.FindObjectsOfType<Card>();

        Debug.Log("Number of cards on field: " + cardsOnField.Length);

        for (int i = 0; i < cardsOnField.Length; i++)
        {
            hasShield = cardsOnField[i].GetComponent<Card>().hasShieldWall;
            inPlay = cardsOnField[i].GetComponent<CardController>().cardInPlay;

            
            if (hasShield && inPlay && cardsOnField[i].gameObject.tag == "Player")
            {
                shieldWall = true;
                networkView.RPC("sendEnemyShieldWall", RPCMode.Others, shieldWall);
                break;
            }


            shieldWall = false;
            networkView.RPC("sendEnemyShieldWall", RPCMode.Others, shieldWall);   
            
        }

        fieldUpdated = false;

    }

    //Method called remotely to show other client card you have instantiated. Just as the other method, database information will be pulled
    //and assigned to the card here.
        
	[RPC] void createCardForEnemy(NetworkViewID viewID, string name, int cardID, int pwr, int str, int vit, int eqp, bool shield, bool qw, bool pierce, bool daze, bool isSpell, string desc, string type) {
		string imageFileString = cardID.ToString ();
		Sprite sprite = Resources.Load<Sprite>(imageFileString);
        if (isSpell)
        {
            card = (GameObject)Instantiate(cardSpellPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
        }
        else if (!isSpell)
        {
            card = (GameObject)Instantiate(cardPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
        }

        card.networkView.viewID = viewID;
		card.transform.Rotate (-90.0f, 180.0f, 0.0f, Space.World);
        StartCoroutine(moveCard(viewID, card.transform.position, gameBoard.calcHandPositionForEnemy(), 0.5f));
		card.GetComponent<Card> ().cardName = name;
        card.GetComponent<Card>().cardStr = str;
        card.GetComponent<Card>().cardVit = vit;
        card.GetComponent<Card>().cardPwr = pwr;
        card.GetComponent<Card>().cardEqp = eqp;
        card.GetComponent<Card>().hasShieldWall = shield;
	    card.GetComponent<Card>().hasQuickWitted = qw;
	    card.GetComponent<Card>().hasPiercing = pierce;
	    card.GetComponent<Card>().cardOrigEqp = eqp;
	    card.GetComponent<Card>().cardOrigPwr = pwr;
	    card.GetComponent<Card>().cardOrigStr = str;
	    card.GetComponent<Card>().cardOrigVit = vit;
        card.GetComponent<Card>().cardSprite = sprite;

        switch (type)
        {
            case "SPELL-N":
                card.GetComponent<Card>().cardType = Card.cardTypes.spellNorm;
                break;
            case "SPELL-E":
                card.GetComponent<Card>().cardType = Card.cardTypes.spellEqp;
                break;
            case "SPELL-T":
                card.GetComponent<Card>().cardType = Card.cardTypes.spellTrig;
                break;
            case "SPELL-C":
                card.GetComponent<Card>().cardType = Card.cardTypes.spellCont;
                break;
        }

		card.tag = "Opponent";

        card.GetComponent<Card>().isSpell = isSpell;
        card.GetComponent<Card>().cardDesc = desc;
        card.GetComponent<Card>().type = type;



		card.tag = "Opponent";
        updateCardStats(viewID, card.GetComponent<Card>().cardName, cardID, pwr, str, vit, eqp, shield, qw, pierce, daze, sprite, isSpell, desc, type);
	}

    [RPC]
    void loadPlayerForEnemy(string enemyDisplayName)
    {
        enemyPlayer.playerUserName = "testName2";
        enemyPlayer.playerUserID = 0000000001;
        enemyPlayer.playerDisplayName = enemyDisplayName;
        enemyPlayer.playerCurrentPower = 1;
        GameObject.Find("enemyNameText").GetComponent<Text>().text = enemyPlayer.playerDisplayName;

    }

    [RPC]
    void updatePowerForEnemy(int powerCurrentAmount, int powerTotalAmount)
    {
        enemyPlayer.playerCurrentPower = powerCurrentAmount;
        enemyPlayer.playerMaxPower = powerTotalAmount;
        GameObject.Find("enemyPowerCurrent").GetComponent<Text>().text = enemyPlayer.playerCurrentPower.ToString();
        GameObject.Find("enemyPowerTotal").GetComponent<Text>().text = enemyPlayer.playerMaxPower.ToString();
    }

    [RPC]
    void updateHealthForEnemy(int healthAmount)
    {
        enemyPlayer.playerHealth = healthAmount;
        enemyHealthSlider.value = enemyPlayer.playerHealth;
        GameObject.Find("enemyHealthValue").GetComponent<Text>().text = enemyPlayer.playerHealth.ToString();
    }

    [RPC]
    void updateEnemyHealth(int healthAmount)
    {
        player.playerHealth = healthAmount;
        playerHealthSlider.value = player.playerHealth;
        GameObject.Find("playerHealthValue").GetComponent<Text>().text = player.playerHealth.ToString();
        networkView.RPC("updateHealthForEnemy", RPCMode.Others, player.playerHealth);
    }


    //Finding a card, given its unique NetworkViewID, and updating its Vitality attribute.
    [RPC]
    public void updateCardVitForEnemy(NetworkViewID cardUpdateID, int newCardVit, string textToUpdate)
    {
        Component[] compArray;
        NetworkView.Find(cardUpdateID).GetComponent<Card>().cardVit = newCardVit;

        compArray = NetworkView.Find(cardUpdateID).GetComponentsInChildren<Text>();

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compArray[i].name == textToUpdate)
            {
                compArray[i].GetComponent<Text>().text = newCardVit.ToString();
            }

        }


        Debug.Log(NetworkView.Find(cardUpdateID).networkView.viewID.ToString());

    }

    [RPC]

    public void updateCardEqp(NetworkViewID viewID, int newCardEqp)
    {
        Component[] compArray;
        NetworkView.Find(viewID).GetComponent<Card>().cardEqp = newCardEqp;

        compArray = NetworkView.Find(viewID).GetComponentsInChildren<Text>();

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compArray[i].name == "equipText")
            {
                compArray[i].GetComponent<Text>().text = newCardEqp.ToString();
            }

        }
    }



    [RPC]
    public void destroyCard(NetworkViewID destroyID)
    {
        //NOTE
        /* During your turn, if a card owned by you that has shield wall attacks and is destroyed in the process, your shield wall boolean
         * will not update until you end your turn. This has no current effect on gameplay, however. Just a note.
         */


        // Write a method to find the closest spot to the middle, and move all cards behind that spot up one spot. (Some form of "bubbling")



		GameObject objToDestroy = NetworkView.Find(destroyID).gameObject;
		objToDestroy.networkView.viewID = destroyID;

        if (isPlayerTurn && objToDestroy.tag == "Player")
        {
            if (objToDestroy.GetComponent<Card>().type.ToLower() == "minion")
            {
                infoManager.setOpponentKills(infoManager.getOpponentKills() + 1);

            }
            for (int i = 0; i < gameBoard.playPosAvail.Length; i++)
            {
                if (objToDestroy.gameObject.GetComponent<CardController>().origPos == gameBoard.cardPlayPositions[i])
                {
                    gameBoard.playPosAvail[i] = true;
                    Debug.Log("Play position " + i + " available again.");
                }

            }
        }

        if (isPlayerTurn && objToDestroy.tag == "Opponent")
        {
            if (objToDestroy.GetComponent<Card>().type.ToLower() == "minion")
            {
                infoManager.setPlayerKills(infoManager.getPlayerKills() + 1);

            } 
            for (int i = 0; i < gameBoard.enemyPlayPosAvail.Length; i++)
            {
                if (objToDestroy.gameObject.GetComponent<CardController>().origPosForEnemy == gameBoard.enemyCardPlayPositions[i])
                {
                    gameBoard.enemyPlayPosAvail[i] = true;
                }

            }
        }

        if (!isPlayerTurn && objToDestroy.tag == "Player")
        {
            if (objToDestroy.GetComponent<Card>().type.ToLower() == "minion")
            {
                infoManager.setOpponentKills(infoManager.getOpponentKills() + 1);

            } 
            for (int i = 0; i < gameBoard.playPosAvail.Length; i++)
            {
                if (objToDestroy.gameObject.GetComponent<CardController>().origPos == gameBoard.cardPlayPositions[i])
                {
                    gameBoard.playPosAvail[i] = true;
                    Debug.Log("Play position " + i + " available again.");
                }

            }
        }

        if (!isPlayerTurn && objToDestroy.tag == "Opponent")
        {
            if (objToDestroy.GetComponent<Card>().type.ToLower() == "minion")
            {
                infoManager.setPlayerKills(infoManager.getPlayerKills() + 1);

            } 
            for (int i = 0; i < gameBoard.enemyPlayPosAvail.Length; i++)
            {
                if (objToDestroy.gameObject.GetComponent<CardController>().origPosForEnemy == gameBoard.enemyCardPlayPositions[i])
                {
                    gameBoard.enemyPlayPosAvail[i] = true;
                }

            }
        }
        objToDestroy.GetComponent<Card>().hasPiercing = false;
        objToDestroy.GetComponent<Card>().hasQuickWitted = false;
        objToDestroy.GetComponent<Card>().hasShieldWall = false;
        if (objToDestroy.GetComponent<Card>().attr == "PI") objToDestroy.GetComponent<Card>().hasPiercing = true;
        if (objToDestroy.GetComponent<Card>().attr == "QW") objToDestroy.GetComponent<Card>().hasQuickWitted = true;
        if (objToDestroy.GetComponent<Card>().attr == "SW") objToDestroy.GetComponent<Card>().hasShieldWall = true;

        objToDestroy.GetComponent<Card>().cardPwr = objToDestroy.GetComponent<Card>().cardOrigPwr;
        objToDestroy.GetComponent<Card>().cardStr = objToDestroy.GetComponent<Card>().cardOrigStr;
        objToDestroy.GetComponent<Card>().cardVit = objToDestroy.GetComponent<Card>().cardOrigVit;
        objToDestroy.GetComponent<Card>().cardEqp = objToDestroy.GetComponent<Card>().cardOrigEqp;

        player.playerDeck.addDiscard(objToDestroy.GetComponent<Card>());
		//StartCoroutine (moveObject (objToDestroy, objToDestroy.transform.position, new Vector3(-1000.0f, 0.0f, -1.0f), 
        Destroy(objToDestroy);
        fieldUpdated = true;
    }

    [RPC]
    void sendEnemyShieldWall(bool shield)
    {
        enemyShieldWall = shield;
    }

    [RPC]
    public void updateCardShieldWall(NetworkViewID viewID, bool shieldWall)
    {
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().hasShieldWall = shieldWall;
    }

    [RPC]
    void swapTurn(bool turnBool)
    {
        player.isTurn = turnBool;
        enemyPlayer.isTurn = false;
    }

    [RPC]
    void getTurn(bool turnBool)
    {
        player.isTurn = turnBool;
    }

    [RPC]
    void startGame()
    {
        if (Application.loadedLevel.Equals(0))
        {
            Application.LoadLevel(2);
            infoManager.playGameMusic();
        }
    }

	[RPC]
	void postGameLobby()
	{
		if (Application.loadedLevel.Equals(2))
		{
            infoManager.setPostGame(true);
			Application.LoadLevel(0);
            infoManager.playMenuMusic();
		}
	}

    // We can use this to animate card movements, I believe.

    public IEnumerator moveCard(NetworkViewID viewID, Vector3 origPos, Vector3 destPos, float time) 
    {
        Debug.Log("Moving card");
        Transform trans = NetworkView.Find(viewID).gameObject.transform;
        float i = 0.0f;
        float rate = 1.0f / time;
        while (i < 1.0f) 
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(origPos, destPos, i);
            yield return null;
        }

    }

    public IEnumerator attackCardMove(NetworkViewID viewID, Vector3 origPos, Vector3 destPos, float time)
    {
        Debug.Log("Moving card");
        
		// NEWER VERSION 
		networkView.RPC("callAttackCardMoveForEnemy", RPCMode.Others, viewID, attackManager.defendID, time);

        Transform trans = NetworkView.Find(viewID).gameObject.transform;
        float i = 0.0f;
        float rate = 1.0f / time;
        
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(origPos, destPos, i);
            yield return null;
        }

        attackManager.attackCard(attackManager.attackID, attackManager.defendID, attackManager.attackCardStr, attackManager.attackCardVit, attackManager.defendCardStr, attackManager.defendCardVit);
        i = 0.0f;

        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(destPos, origPos, i);
            yield return null;
        }

        yield return new WaitForSeconds(0.10f);


        StartCoroutine(checkToDestroy(attackManager.attackID));
        StartCoroutine(checkToDestroy(attackManager.defendID));


    }

    [RPC]
    public void callAttackCardMoveToPlayerForEnemy(NetworkViewID viewID, Vector3 destPos, float time)
    {
        StartCoroutine(attackCardMoveToPlayerForEnemy(viewID, NetworkView.Find(viewID).GetComponent<CardController>().origPosForEnemy, destPos, time));
    }

    public IEnumerator attackCardMoveToPlayerForEnemy(NetworkViewID viewID, Vector3 origPos, Vector3 destPos, float time)
    {
        StartCoroutine(moveCard(viewID, origPos, destPos, time));
        new WaitForSeconds(time);
        yield return null;
        StartCoroutine(moveCard(viewID, destPos, origPos, time));
    }

    public IEnumerator attackCardMoveToPlayer(NetworkViewID viewID, Vector3 origPos, Vector3 destPos, float time)
    {
        Debug.Log("Moving card");
        Transform trans = NetworkView.Find(viewID).gameObject.transform;
        float i = 0.0f;
        float rate = 1.0f / time;

        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(origPos, destPos, i);
            yield return null;
        }

        attackManager.attackPlayer(attackManager.attackID, attackManager.attackCardStr, enemyPlayer.playerHealth);
        NetworkView.Find(attackManager.attackID).GetComponent<CardController>().hasAttacked = true;
        NetworkView.Find(attackManager.attackID).gameObject.transform.localScale = NetworkView.Find(attackManager.attackID).GetComponent<CardController>().origScale;

        StartCoroutine(moveCard(viewID, NetworkView.Find(viewID).gameObject.transform.position, origPos, 0.25f));
    }

	public IEnumerator moveObject(GameObject obj, Vector3 origPos, Vector3 destPos, float time) 
	{
		Debug.Log ("Moving object");
		float i = 0.0f;
		float rate = 1.0f / time;
		while (i < 1.0f) 
		{
			i += Time.deltaTime * rate;
			obj.transform.localPosition = Vector3.Lerp (origPos, destPos, i);
			Debug.Log ("Object in motion");
			yield return null;
		}

        Debug.Log("Object moved");

	}

	public IEnumerator waitForPanel(float time) 
	{
		yield return new WaitForSeconds (time);
		Debug.Log (panelInPlace.ToString ());
        panelInPlace = true;
	}
    
    [RPC]
    public void updateCardStr(NetworkViewID viewID, int newStr)
    {
        Component[] compArray;
        NetworkView.Find(viewID).gameObject.GetComponent<Card>().cardStr = newStr;

        compArray = NetworkView.Find(viewID).GetComponentsInChildren<Text>();

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compArray[i].name == "strengthText")
            {
                compArray[i].GetComponent<Text>().text = newStr.ToString();
            }

        }
    }
 

	[RPC] void destroyGameManager() 
	{
		Destroy(GameObject.Find("GameSceneManagers"));
    }

    #endregion

}
