﻿using UnityEngine;
using System.Collections;

public class LeechLife : Spell, ISpell {

    GameManager gameManager;
    InfoManager infoManager;
    LeechLife leechLifeSpell;
    private bool spellIsCast = false;
    private const int DMG_AMOUNT = 1;
    private const int HEAL_AMOUNT = 2;
    GameObject targetObject;

	// Use this for initialization
	void Start () 
    {
        targetObject = new GameObject();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        leechLifeSpell = new LeechLife();
        leechLifeSpell.spellName = "Leech Life";
        leechLifeSpell.spellType = SpellTypes.spellNorm;
	}
	
	// Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<CardController>().cardInPlay && !spellIsCast)
        {
            gameObject.GetComponent<CardController>().findPosInHand();
            gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo = new RaycastHit();

            if (Physics.Raycast(ray, out hitInfo))
            {
                targetObject = hitInfo.collider.gameObject.transform.parent.gameObject;
                Debug.Log(hitInfo.collider.gameObject.transform.parent.gameObject.name);

                if (targetObject.gameObject.tag == "Player" && targetObject != this.gameObject)
                {
                    castSpell();
                }

                else
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, gameObject.GetComponent<CardController>().origPos, 0.2f));
                    gameManager.showError(6);
                    gameObject.GetComponent<CardController>().cardInPlay = false;
                    gameObject.GetComponent<CardController>().cardSnapped = false;
                    gameObject.GetComponent<CardController>().cardInHand = true;
                    gameManager.addPlayerPower(gameObject.GetComponent<Card>().cardPwr);
                    gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = false;
                    spellIsCast = false;
                }
            }
        }
    }


    public void castSpell()
    {
        int newVit = targetObject.GetComponent<Card>().cardVit - DMG_AMOUNT;
        targetObject.GetComponent<Card>().cardVit = newVit;
        gameManager.networkView.RPC("updateCardVitForEnemy", RPCMode.All, targetObject.networkView.viewID, newVit, "vitalityText");
        gameManager.addPlayerHP(HEAL_AMOUNT);

        if (targetObject.GetComponent<Card>().cardVit <= 0)
        {
            gameManager.networkView.RPC("destroyCard", RPCMode.All, targetObject.networkView.viewID);
        }


        GameObject spellIcon;
        Vector3 spellTokenPos;
        spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
        spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
        spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
        spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
        spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
        spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
        spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
        spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
        spellIcon.gameObject.tag = "PlayerSpell";

        infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

        gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
        //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

        gameManager.networkView.RPC("destroyCard", RPCMode.All, gameObject.networkView.viewID);
        spellIsCast = true;

    }

}
