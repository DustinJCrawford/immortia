﻿using UnityEngine;
using System.Collections;

public class SuitUp : Spell, ISpell {

    GameManager gameManager;
    InfoManager infoManager;
    SuitUp suitUpSpell;
    private bool spellIsCast = false;
    private const int STR_INCREASE = 1;
    private const int VIT_INCREASE = 1;
    private const int EQP_COST = 1;
    GameObject targetObject;


	// Use this for initialization
	void Start () 
    {
        targetObject = new GameObject();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        suitUpSpell = new SuitUp();
        suitUpSpell.spellName = "Suit Up!";
        suitUpSpell.spellType = SpellTypes.spellEqp;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (gameObject.GetComponent<CardController>().cardInPlay && !spellIsCast)
        {
            gameObject.GetComponent<CardController>().findPosInHand();
            gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo = new RaycastHit();

            if (Physics.Raycast(ray, out hitInfo))
            {
                targetObject = hitInfo.collider.gameObject.transform.parent.gameObject;


                if (targetObject.GetComponent<Card>().cardEqp >= EQP_COST)
                {
                    castSpell();
                }

                else if (targetObject.GetComponent<Card>().cardEqp < EQP_COST)
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, gameObject.GetComponent<CardController>().origPos, 0.2f));
                    gameManager.showError(9);
                    gameObject.GetComponent<CardController>().cardInPlay = false;
                    gameObject.GetComponent<CardController>().cardSnapped = false;
                    gameObject.GetComponent<CardController>().cardInHand = true;
                    gameManager.addPlayerPower(gameObject.GetComponent<Card>().cardPwr);
                    gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = false;
                    spellIsCast = false;
                }
            }
        }
	}

    public void castSpell()
    {
        int newStr = targetObject.GetComponent<Card>().cardStr + STR_INCREASE;
        int newVit = targetObject.GetComponent<Card>().cardVit + VIT_INCREASE;
        int newEqp = targetObject.GetComponent<Card>().cardEqp - EQP_COST;
        targetObject.GetComponent<Card>().cardStr = newStr;
        targetObject.GetComponent<Card>().cardVit = newVit;
        targetObject.GetComponent<Card>().cardEqp = newEqp;
        gameManager.networkView.RPC("updateCardStr", RPCMode.All, targetObject.networkView.viewID, newStr);
        gameManager.networkView.RPC("updateCardEqp", RPCMode.All, targetObject.networkView.viewID, newEqp);
        gameManager.networkView.RPC("updateCardVitForEnemy", RPCMode.All, targetObject.networkView.viewID, newVit, "vitalityText");


        GameObject spellIcon;
        Vector3 spellTokenPos;
        spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
        spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
        spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
        spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
        spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
        spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
        spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
        spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
        spellIcon.gameObject.tag = "PlayerSpell";

        infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

        gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
        //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

        gameManager.networkView.RPC("destroyCard", RPCMode.All, gameObject.networkView.viewID);
        spellIsCast = true;
    }
}
