﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class DeckBuilder : MonoBehaviour {
    
    //game objects
    public InfoManager infoManager;
    //public DeckBuilderMenuController deckBuilderMenuController;
	public GameObject loadingPanel;
    public GameObject cardButtonMinion;
    public GameObject cardButtonSpell;
    public GameObject deckSelectionObject;
    public GameObject deckSelectionPanel;
    public GameObject buttonPrefab;
    public GameObject editDeckButton;
    public GameObject deleteDeckButton;
    public GameObject deckSelectScrollBar;
    public GameObject spellDescPanel;
    public Transform availCardsPanel;
    public Transform cardsInDeckPanel;
    public Transform deckPanel;
    public InputField deckname;
    public InputField searchValue;
    public Toggle searchDeck;
    public Text currentDeckName;

    //bools, will probably be replaced with alternative
    bool newDeck = false;
    bool editingDeck = false;

    //Data structures for initialized panels
    //and moving data between panels
    public List<Card> cardsToList;
    public List<Card> cardsToDeck;
    List<Card> userCardList;
    List<Card> deckToEdit;

    public Text minionCountText;
    public Text normalCountText;
    public Text equipCountText;
    public Text triggerCountText;
    public Text continuousCountText;
    public Text cardCountText;

    public Text zeroPowerText;
    public Text onePowerText;
    public Text twoPowerText;
    public Text threePowerText;
    public Text fourPowerText;
    public Text fivePowerText;

    //ADD POWER BREAKDOWN NEXT
    void updateDeckBreakdown()
    {
        int minionCount = 0;
        int normalCount = 0;
        int equipCount = 0;
        int triggerCount = 0;
        int continuousCount = 0;
        int cardCount = 0;

        int zeroPowerCount = 0;
        int onePowerCount = 0;
        int twoPowerCount = 0;
        int threePowerCount = 0;
        int fourPowerCount = 0;
        int fivePowerCount = 0;

        foreach(Card card in deckToEdit)
        {
            switch(card.cardType)
            {
                case Card.cardTypes.minionCard: minionCount++;
                    break;
                case Card.cardTypes.spellNorm: normalCount++;
                    break;
                case Card.cardTypes.spellEqp: equipCount++;
                    break;
                case Card.cardTypes.spellTrig: triggerCount++;
                    break;
                case Card.cardTypes.spellCont: continuousCount++;
                    break;
                default:
                    break;
            }
            cardCount++;

            switch (card.cardPwr)
            {
                case 0: zeroPowerCount++;
                    break;
                case 1: onePowerCount++;
                    break;
                case 2: twoPowerCount++;
                    break;
                case 3: threePowerCount++;
                    break;
                case 4: fourPowerCount++;
                    break;
                case 5: fivePowerCount++;
                    break;
                default:
                    break;
            }
        }

        minionCountText.text = minionCount.ToString();
        normalCountText.text = normalCount.ToString();
        equipCountText.text = equipCount.ToString();
        triggerCountText.text = triggerCount.ToString();
        continuousCountText.text = continuousCount.ToString();
        cardCountText.text = cardCount.ToString();

        zeroPowerText.text = zeroPowerCount.ToString();
        onePowerText.text = onePowerCount.ToString();
        twoPowerText.text = twoPowerCount.ToString();
        threePowerText.text = threePowerCount.ToString();
        fourPowerText.text = fourPowerCount.ToString();
        fivePowerText.text = fivePowerCount.ToString();

    }

	// Use this for initialization
    void Start()
    {
        gameObject.AddComponent<Card>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        deckSelectionPanel.SetActive(true);
        editDeckButton.SetActive(false);
        deleteDeckButton.SetActive(false);
        infoManager.setDeckID(0);
        getDecks();
    }

    //load the deck selection panel 
    //with decks owned by the player
    private void getDecks()
    {
        initializeComponents();

        string[] deckStuff = infoManager.getDeckInfo();
        if (deckStuff.Length > 11)
        {
            deckSelectScrollBar.SetActive(true);
        }
        for (int i = 0; i < deckStuff.Length - 1; i++)
        {
            string[] temp = deckStuff[i].Split(';');
            GameObject button = (GameObject)Instantiate(buttonPrefab);
            button.GetComponentInChildren<Text>().text = temp[1];
            button.GetComponent<Button>().onClick.AddListener(() =>
                {
                    infoManager.setDeckID(int.Parse(temp[0]));
                    infoManager.setDeckName(temp[1]);
                    editDeckButton.SetActive(true);
                    deleteDeckButton.SetActive(true);
                });
            button.transform.SetParent(deckPanel, false);
        }
    }

    void initializeComponents()
    {
        //empty current list so as to not relist the same lobbies more than once
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in deckPanel) children.Add(child.gameObject);
        if (children != null)
        {
            children.ForEach(child => Destroy(child));
        }
    }

    void clearPanels()
    {
        clearPanel(availCardsPanel);
        clearPanel(cardsInDeckPanel);
    }

    //move cards from the owned cards
    //to the deck list
	public void addCardsToUserDeck()
	{
		for (int i = 0; i < cardsToDeck.Count; i++) 
		{
			Card newCard = cardsToDeck[i];
            deckToEdit.Add(newCard);

            for (int j = 0; j < userCardList.Count; j++)
            {
                if (cardsToDeck[i].cardName == userCardList[j].cardName)
                {
                    userCardList.RemoveAt(j);
                    break;
                }
            }
        }

        clearPanels();
        sortDeckByName(deckToEdit, cardsInDeckPanel);
        sortDeckByName(userCardList, availCardsPanel);
	}

    //move cards from the deck list
    //to the owned cards list
    public void addCardsToUserList()
    {
        for (int i = 0; i < cardsToList.Count; i++)
        {
            Card newCard = cardsToList[i];
            userCardList.Add(newCard);

            for (int j = 0; j < deckToEdit.Count; j++)
            {
                if (cardsToList[i].cardName == deckToEdit[j].cardName)
                {
                    deckToEdit.RemoveAt(j);
                    break;
                }
            }
        }

        clearPanels();
        sortDeckByName(deckToEdit, cardsInDeckPanel);
        sortDeckByName(userCardList, availCardsPanel);
    }

    //start with full owned card list
    //and empty deck list
	public void createNewDeck()
	{
		deckSelectionPanel.SetActive(false);
		loadingPanel.SetActive (true);
        clearAllValues();
		newDeck = true;
        editingDeck = false;
        StartCoroutine(getOwnedCards(infoManager.getUserID()));
        deckname.text = string.Empty;
	}

    //populate deck list with current selected deck values
    //populate available cards list with difference 
    //of owned cards list and selected deck values
	public void editExistingDeck() 
	{
        clearAllValues();
		editingDeck = true;
        newDeck = false;
		deckSelectionPanel.SetActive (false);
		loadingPanel.SetActive (true);
		StartCoroutine (loadDeck ());
        deckname.text = infoManager.getDeckName();
        currentDeckName.text = infoManager.getDeckName();
	} 

    //find the differences in the two lists
    //add two lists to the panels
	void populateLists() 
	{
        clearPanels();

        for(int i = 0; i < deckToEdit.Count; i++)
        {
            for (int j = 0; j < userCardList.Count; j++)
            {
                if (userCardList[j].cardName == deckToEdit[i].cardName)
                {
                    userCardList.RemoveAt(j);
                    break;
                } 
            }
        }

        sortDeckByName(deckToEdit, cardsInDeckPanel);
        sortDeckByName(userCardList, availCardsPanel);
	}

    public void clearAllValues()
    {
        clearPanels();
        deckToEdit = new List<Card>();
        userCardList = new List<Card>();
    }

    public void searchCards()
    {
        Transform parent;
        List<Card> cardsFound = new List<Card>();
        List<Card> compareList;

        if (searchDeck.isOn)
        {
            parent = cardsInDeckPanel;
            compareList = new List<Card>(deckToEdit);
        }
        else
        {
            parent = availCardsPanel;
            compareList = new List<Card>(userCardList);
        }
        for (int i = 0; i < compareList.Count; i++)
        {
            if(compareList[i].cardName.ToLower().Contains(searchValue.text.ToLower()))
            {
                cardsFound.Add(compareList[i]);
            }
        }

        clearPanel(parent);
        sortDeckByName(cardsFound, parent);
    }

    public void clearSearch()
    {
        populateLists();
        searchValue.text = string.Empty;
    }
    //method to sort decks by name and then add to panels
    void sortDeckByName(List<Card> unsortedDeck, Transform parent)
    {
        List<Card> sortedDeck = new List<Card>();

        IEnumerable<Card> query =
        unsortedDeck.OrderBy(userSortedDeck => userSortedDeck.cardName);

        foreach (Card card in query)
        {
            sortedDeck.Add(card);
        }

        addDeckToPanel(sortedDeck, parent);
    }

    //method for adding available cards and
    //selected deck values to their 
    //corresponding parent
    void addDeckToPanel(List<Card> deckOfCards, Transform parent)
    {
        for (int i = 0; i < deckOfCards.Count; i++)
        {
            addCardToPanel(deckOfCards[i], parent);
        }

        updateDeckBreakdown();
    }

    void addCardToPanel(Card cardToAdd, Transform parent)
    {

        Component[] compArray;
        GameObject button;
        if (!cardToAdd.isSpell)
        {
            button = (GameObject)Instantiate(cardButtonMinion);
            button.transform.SetParent(parent);
            button.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            //button.GetComponent<Image>().enabled = false;
            button.AddComponent<Card>();

            addCardAttributes(button, cardToAdd);

            //Make this its own method eventually
            compArray = button.GetComponentsInChildren<Text>();

        
            for (int j = 0; j < compArray.Length; j++)
            {
                switch (compArray[j].name)
                {
                    case "nameText":
                        compArray[j].GetComponent<Text>().text = cardToAdd.cardName;
                        break;
                    case "powerText":
                        compArray[j].GetComponent<Text>().text = "PWR: " + cardToAdd.cardPwr.ToString();
                        break;
                    case "typeText":
                        compArray[j].GetComponent<Text>().text = translateType(cardToAdd.cardType.ToString());
                        break;
                    case "strengthText":
                        compArray[j].GetComponent<Text>().text = "STR: " + cardToAdd.cardStr.ToString();
                        break;
                    case "vitalityText":
                        compArray[j].GetComponent<Text>().text = "VIT: " + cardToAdd.cardVit.ToString();
                        break;
                    case "equipText":
                        compArray[j].GetComponent<Text>().text = "EQP: " + cardToAdd.cardEqp.ToString();
                        break;
                    default: break;
                }
            }
        }
        else if(cardToAdd.isSpell)
        {
            button = (GameObject)Instantiate(cardButtonSpell);
            button.transform.SetParent(parent);
            button.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            //button.GetComponent<Image>().enabled = false;
            button.AddComponent<Card>();

            addCardAttributes(button, cardToAdd);

            //Make this its own method eventually
            compArray = button.GetComponentsInChildren<Text>();
            for (int j = 0; j < compArray.Length; j++)
            {
                switch (compArray[j].name)
                {
                    case "nameText":
                        compArray[j].GetComponent<Text>().text = cardToAdd.cardName;
                        break;
                    case "powerText":
                        compArray[j].GetComponent<Text>().text = "PWR: " + cardToAdd.cardPwr.ToString();
                        break;
                    case "typeText":
                        compArray[j].GetComponent<Text>().text = translateType(cardToAdd.cardType.ToString());
                        break;
                    case "descText":
                        compArray[j].GetComponent<Text>().text = cardToAdd.cardDesc;
                        break;
                    default: break;
                }
            }
        }
    }

    void clearPanel(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }

    IEnumerator getOwnedCards(int userid)
    {
        string formText = string.Empty;
        string url = "http://50.4.16.27:80/loadOwnedCards.php";
		//string url = "http://50.4.16.27:80/loadAllCards.php";
        WWWForm form = new WWWForm();
        form.AddField("myform_hash", "xC6VsGdaF5RkqzT");
        form.AddField("myform_userid", userid);
        WWW w = new WWW(url, form);
        yield return w;

        if (w.error != null)
        {
            print(w.error);
        }
        else
        {
            formText = w.text;
            w.Dispose();

            string[] cards = formText.Split('\n');
            for (int i = 1; i < cards.Length - 1; i++)
            {
                string[] temp = cards[i].Split(';');
                bool spell;
                switch (temp[7])
                {
                    case "SPELL-N":
                    case "SPELL-T":
                    case "SPELL-C":
                    case "SPELL-E":
                        spell = true;
                        break;
                    default:
                        spell = false;
                        break;
                }

                Card newCard = new Card(int.Parse(temp[0]), temp[1], int.Parse(temp[2]), int.Parse(temp[3]), int.Parse(temp[4]),
                   int.Parse(temp[5]), temp[6], temp[8], spell, temp[7]);

                this.userCardList.Add(newCard);
            }

			populateLists ();
			loadingPanel.SetActive (false);
        }
    }

    //Loads a player's deck for a given deckID (set in infoManager). Want to be able to use the Deck class' getDeck() method.
    IEnumerator loadDeck()
    {
        //CARD_ID | CARD_NAME | CARD_PWR | CARD_STR | CARD_VIT | CARD_EQP | CARD_ATRB | CARD_TYPE | CARD_DESC
        //temp[0] | temp[1]   | temp[2]  | temp[3]  | temp[4]  | temp[5]  | temp[6]   | temp[7]   | temp[8]
        string formText;
        string url = "http://50.4.16.27:80/loadDeck.php";
        WWWForm form = new WWWForm();
        form.AddField("myform_deck", infoManager.getDeckID());
        WWW w = new WWW(url, form);
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang

        formText = w.text; //here we return the data our PHP told us
        string[] cards = formText.Split('\n');
        w.Dispose(); //clear our form in game
        formText = string.Empty;
        
        for (int i = 1; i < cards.Length - 1; i++)
        {
            string[] temp = cards[i].Split(';');
            bool spell;
            switch (temp[7])
            {
                case "SPELL-N": 
                case "SPELL-T": 
                case "SPELL-C": 
                case "SPELL-E":
                    spell = true;
                    break;
                default:
                    spell = false;
                    break;
            }

            Card newCard = new Card(int.Parse(temp[0]), temp[1], int.Parse(temp[2]), int.Parse(temp[3]), int.Parse(temp[4]),
                int.Parse(temp[5]), temp[6], temp[8], spell, temp[7]);

            this.deckToEdit.Add(newCard);
        }

		StartCoroutine(getOwnedCards(infoManager.getUserID()));
    }

    string translateType(string text)
    {
        string returnString;
        switch (text)
        {
            case "minionCard": returnString = "Minion";
                break;
            case "spellNorm": returnString = "Spell (N)";
                break;
            case "spellEqp": returnString = "Spell (E)";
                break;
            case "spellCont": returnString = "Spell (C)";
                break;
            case "spellTrig": returnString = "Spell (T)";
                break;
            default:
                returnString = "dafuq?";
                break;
        }
        return returnString;
    }

    public void addCardAttributes(GameObject obj, Card cardToAdd)
    {
        if (cardToAdd.isSpell)
        {
            obj.GetComponent<Card>().cardDesc = cardToAdd.cardDesc;

        }
        else if (!cardToAdd.isSpell)
        {
            obj.GetComponent<Card>().cardStr = cardToAdd.cardStr;
            obj.GetComponent<Card>().cardVit = cardToAdd.cardVit;
            obj.GetComponent<Card>().cardEqp = cardToAdd.cardEqp;
        }
        obj.GetComponent<Card>().isSpell = cardToAdd.isSpell;
        obj.GetComponent<Card>().cardType = cardToAdd.cardType;
        obj.GetComponent<Card>().cardName = cardToAdd.cardName;
        obj.GetComponent<Card>().cardID = cardToAdd.cardID;
        obj.GetComponent<Card>().cardPwr = cardToAdd.cardPwr;
    }

    public void saveNewDeck()
    {
        string newName = deckname.text.Trim();
        string message = "**Deck name must not be empty or special characters**";
        int[] cards = new int[cardsInDeckPanel.childCount];

        for(int i = 0; i < cardsInDeckPanel.childCount; i++)
        {
            cards[i] = cardsInDeckPanel.GetChild(i).GetComponent<Card>().cardID;
        }

        if (this.newDeck)
        {
            if (newName != string.Empty)
            {
                StartCoroutine(createNewDeckInDatabase(infoManager.getUserID(), infoManager.getDeckID(), newName, cards, "insertNewDeck.php"));
            }
            else
            {
                print(message);
            }
        }
        else if (this.editingDeck)
        {
            if (newName != string.Empty)
            {
                StartCoroutine(createNewDeckInDatabase(infoManager.getUserID(), infoManager.getDeckID(), newName, cards, "dropDeckInsertDeck.php"));
            }
            else
            {
                print(message);
            }
        }

        editDeckButton.SetActive(false);
        deleteDeckButton.SetActive(false);
    }

    public void deleteDeck()
    {
        StartCoroutine(deleteSelectedDeck(infoManager.getDeckID()));
    }

    IEnumerator deleteSelectedDeck(int deckid)
    {
        string url = "http://50.4.16.27:80/deleteDeck.php";
        string hash = "xC6VsGdaF5RkqzT";
        string formText = string.Empty;//this field is where the messages sent by PHP script will be in

        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_deckid", deckid);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang

        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim();
            print(formText);
            w.Dispose();
        }

        StartCoroutine(getDeckIDs(infoManager.getUserID()));
    }

    //COROUTINE FOR GETTING DECK IDS ASSOCIATED WITH SIGNED IN USERNAME
    IEnumerator getDeckIDs(int userID)//, Transform parent)
    {
        string url = "http://50.4.16.27:80/getDeckIDs.php";
        string[] deckIDs = new string[10];
        string formText = string.Empty;
        string hash = "xC6VsGdaF5RkqzT";

        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_userID", userID);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang

        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text;//.Trim(); //here we return the data our PHP told us
            string[] deckStuff = formText.Split('\n');
            infoManager.setDeckInfo(deckStuff);
        }

        w.Dispose();
        getDecks();
    }

    IEnumerator createNewDeckInDatabase(int userid, int deckid, string deckname, int[] cards, string phpFile)
    {
        string url = "http://50.4.16.27:80/" + phpFile; //change for your URL
        string hash = "xC6VsGdaF5RkqzT"; 
        string formText = "";//this field is where the messages sent by PHP script will be in
        int newDeckID = 0;

        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_userid", infoManager.getUserID());
        form.AddField("myform_deckid", deckid);
        form.AddField("myform_deckname", deckname);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang

        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim(); //here we return the data our PHP told us
            print(formText);
            newDeckID = int.Parse(formText);
            w.Dispose(); //clear our form in game
        }

        for(int i = 0; i < cards.Length; i++)
        {
            StartCoroutine(saveCardToDeck(cards[i], newDeckID));
        }

        StartCoroutine(getDeckIDs(infoManager.getUserID()));
    }

    IEnumerator saveCardToDeck(int cardid, int deckid)
    {
        string url = "http://50.4.16.27:80/saveCardToDeck.php"; //change for your URL
        string hash = "xC6VsGdaF5RkqzT"; //change your secret code, and remember to change into the PHP file too
        string formText = string.Empty;//this field is where the messages sent by PHP script will be in

        WWWForm form = new WWWForm(); //here you create a new form connection
        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
        form.AddField("myform_cardid", cardid);
        form.AddField("myform_deckid", deckid);
        WWW w = new WWW(url, form); //here we create a var called 'w' and we sync with our URL and the form
        yield return w; //we wait for the form to check the PHP file, so our game dont just hang

        if (w.error != null)
        {
            print(w.error); //if there is an error, tell us
        }
        else
        {
            formText = w.text.Trim(); //here we return the data our PHP told us
            print(formText);
            w.Dispose(); //clear our form in game
        }
    }
}