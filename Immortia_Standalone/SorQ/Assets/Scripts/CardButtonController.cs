﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CardButtonController : MonoBehaviour {

    public DeckBuilder deckBuilder;
    public Transform parentPanel;
    public Image buttonImage;
    public bool buttonSelected;
    //public bool itemSelected;
    public Color selectedColor;
    public Color unselectedColor;// = gameObject.GetComponent<Image>().color;

    private Outline buttonOutline;

	// Use this for initialization
	void Start () {
        buttonSelected = false;
        unselectedColor = gameObject.GetComponent<Image>().color;
        selectedColor = new Color(unselectedColor.r - 0.2f, unselectedColor.g - 0.2f, unselectedColor.b - 0.2f, unselectedColor.a);
        //buttonImage.enabled = false;
        //buttonOutline = gameObject.GetComponent<Outline>();
        //buttonOutline.enabled = false;

        // Had to do this AGAIN, even though I've got the DeckBuilder object declared public at the top of this class, and have been dragging it
        // into the correct field in the editor. So weird.
        deckBuilder = GameObject.Find("Deck Builder Manager").GetComponent<DeckBuilder>();
		parentPanel = this.transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void toggleSelect()
    {
        if (!buttonSelected)
        {
            buttonImage.color = selectedColor;
            buttonSelected = true;
            //itemSelected = true;

			if (parentPanel.name == "buttonPanel") 
			{
				deckBuilder.cardsToDeck.Add (gameObject.GetComponent<Card>());
			}

			else if (parentPanel.name == "buttonDeckPanel") 
			{
				deckBuilder.cardsToList.Add (gameObject.GetComponent<Card>());
			}

            //deckBuilder.cardsToMove.Add(gameObject.GetComponent<Card>());
            //buttonOutline.enabled = true;

        }

        else if (buttonSelected)
        {
            //buttonOutline.enabled = false;
            //buttonImage.enabled = false;
            buttonSelected = false;
            buttonImage.color = unselectedColor;
            //itemSelected = false;

			if (parentPanel.name == "buttonPanel") 
			{
				deckBuilder.cardsToDeck.Remove (gameObject.GetComponent<Card>());
			}

			else if (parentPanel.name == "buttonDeckPanel") 
			{
				deckBuilder.cardsToList.Remove (gameObject.GetComponent<Card>());
			}

            //deckBuilder.cardsToMove.Remove(gameObject.GetComponent<Card>());
        }
    }

}
