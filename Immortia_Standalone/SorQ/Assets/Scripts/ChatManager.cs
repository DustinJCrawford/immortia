﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChatManager : MonoBehaviour
{
    public InfoManager infoManager;
    public InputField chatInputField;
    public Transform chatPanel;
    public GameObject chatPanelObject;
    public GameObject chatInputFieldObject;
    public GameObject textPrefab;
    public GameObject chatScrollBar;
    public AudioSource clickSource;
    public int chatCount = 0;
    public int counter = 0;

    void Awake()
    {
        InitializeChat();
    }

    void Start()
    {
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Return) && (chatPanelObject.activeSelf == true))
        {
            HandleNewEntries();
        }
    }

    public void setActive()
    {
        if (chatPanelObject.activeSelf == true)
        {
            chatPanelObject.SetActive(false);
            chatInputFieldObject.SetActive(false);
        }
        else
        {
            chatPanelObject.SetActive(true);
            chatInputFieldObject.SetActive(true);
            InitializeChat();
        }
    }

    void InitializeChat()
    {
        chatInputField.text = string.Empty;
        chatInputField.Select();
    }

    public void HandleNewEntries()
    {
        if (chatInputField.text.Length <= 0)
        {
            chatInputField.Select();
        }
        else
        {
            string msg = chatInputField.text.TrimEnd();
            chatCount += msg.Length % 70;
            GameObject chatMessage = (GameObject)Instantiate(textPrefab);
            chatMessage.GetComponent<Text>().text = "[" + System.DateTime.Now.ToString("hh:mm:ss") + "] " + infoManager.getDisplayName() + ": " + msg;
            chatMessage.transform.SetParent(chatPanel, false);
            clickSource.Play();
            this.networkView.RPC("AddChatEntry", RPCMode.Others, infoManager.getDisplayName(), msg);
        }
        if (chatPanel.GetComponent<Image>().rectTransform.sizeDelta.y >= 1)
        {
            chatScrollBar.SetActive(true);
        }
        if (chatCount > 150)
        {
            GameObject firstChild = chatPanel.transform.GetChild(0).gameObject;
            Destroy(firstChild);
        }
        InitializeChat();
    }

    [RPC]
    void AddChatEntry(string name, string msg)
    {
        GameObject chatMessage = (GameObject)Instantiate(textPrefab);
        chatMessage.GetComponent<Text>().text = "[" + System.DateTime.Now.ToString("hh:mm:ss") + "] " + name + ": " + msg;
        chatCount += msg.Length / 70;
        chatMessage.transform.SetParent(chatPanel, false);
        clickSource.Play();
        if (chatPanel.GetComponent<Image>().rectTransform.sizeDelta.y >= 1)
        {
            chatScrollBar.SetActive(true);
        }
        if (chatCount > 150)
        {
            GameObject firstChild = chatPanel.transform.GetChild(0).gameObject;
            Destroy(firstChild);
        }
    }
}

