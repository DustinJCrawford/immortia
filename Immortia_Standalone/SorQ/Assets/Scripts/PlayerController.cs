﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


    GameManager gameManager;
    AttackManager attackManager;



	// Use this for initialization
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        attackManager = GameObject.Find("AttackManager").GetComponent<AttackManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseOver()
    {
        if (gameObject.tag == "PlayerHand")
        {
            Debug.Log("Entered Player Hand");
        }

        if (gameObject.tag == "OpponentHand")
        {
            Debug.Log("Entered Opponent Hand");
        }
    }

    void OnMouseDown()
    {
        if (gameObject.tag == "PlayerHand")
        {
            //Do some other stuff

            Debug.Log("Player Hand clicked.");
        }

        if (gameObject.tag == "enemyPlayer")
        {

            Debug.Log(attackManager.attackID.ToString());
            Debug.Log(attackManager.readyToAttack.ToString());

            if (!gameManager.enemyShieldWall)// && attackManager.readyToAttack)
            {

                if (!NetworkView.Find(attackManager.attackID).GetComponent<CardController>().hasAttacked)
                {
                    Debug.Log("Attack Enemy Player!");
                    StartCoroutine(gameManager.attackCardMoveToPlayer(attackManager.attackID, NetworkView.Find(attackManager.attackID).GetComponent<CardController>().origPos, this.gameObject.transform.position, 0.25f));
                    gameManager.networkView.RPC("callAttackCardMoveToPlayerForEnemy", RPCMode.Others, attackManager.attackID, new Vector3(-800, -150, -1), 0.25f);
                    //attackManager.attackPlayer(attackManager.attackID, attackManager.attackCardStr, gameManager.enemyPlayer.playerHealth);
                    //NetworkView.Find(attackManager.attackID).GetComponent<CardController>().hasAttacked = true;
                    //NetworkView.Find(attackManager.attackID).gameObject.transform.localScale = NetworkView.Find(attackManager.attackID).GetComponent<CardController>().origScale;
                }


                else gameManager.showError(5);

            }


            else if (gameManager.enemyShieldWall)
            {
                gameManager.showError(6);
            }

            Debug.Log("Enemy Player Portrait clicked!");
        }
    }

    void OnMouseExit()
    {
        if (gameObject.tag == "PlayerHand")
        {
            Debug.Log("Left Player Hand.");
        }

        if (gameObject.tag == "OpponentHand")
        {
            Debug.Log("Left Opponent Hand");
        }
    }
}
