﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class setResolution : MonoBehaviour {

	private Resolution[] resolutions;
	private Resolution startingResolution;
	private bool isFullScreen;
	private bool startScreenFull;
    public Transform availableResPanel;
    public GameObject buttonPrefab;
	public GameObject scrollBar;
    public AudioSource clickSource;
	public Text fullWindow;


	// Use this for initialization
	void Start() 
    {
		if (PlayerPrefs.GetString ("fullScreen", "true") == "true") {
						isFullScreen = true;
						fullWindow.text = "Window";
				} else {
						isFullScreen = false;
						fullWindow.text = "Full Screen";
				}

		startScreenFull = isFullScreen;
        startingResolution.width = PlayerPrefs.GetInt("Resolution Width", 1280);
        startingResolution.height = PlayerPrefs.GetInt("Resolution Height", 720);

        Screen.SetResolution(startingResolution.width, startingResolution.height, startScreenFull);
        showResolutions();
	}

    public void resetResolution()
    {
        Screen.SetResolution(startingResolution.width, startingResolution.height, startScreenFull);
		if (startScreenFull) {
						fullWindow.text = "Window";
				} else {
						fullWindow.text = "Full Screen";
				}
    }

    public void updateResolution()
    {
		string full;
        PlayerPrefs.SetInt("Resolution Width", Screen.width);
        PlayerPrefs.SetInt("Resolution Height", Screen.height);
		if (isFullScreen) {
						full = "true";
				} else {
						full = "false";
				}

		PlayerPrefs.SetString ("fullScreen", full);
        startingResolution.width = PlayerPrefs.GetInt("Resolution Width");
        startingResolution.height = PlayerPrefs.GetInt("Resolution Height");
		if (PlayerPrefs.GetString ("fullScreen", "true") == "true") {
						startScreenFull = true;
						fullWindow.text = "Window";
						} else {startScreenFull = false;
								fullWindow.text = "Full Screen";
				}
    }
    public void showResolutions()
    {
        resolutions = Screen.resolutions;
		int resCounter = 0;
        for(int i = 0; i < resolutions.Length; i++)
        {
			if(resolutions[i].width >= 1280){
				resCounter++;
				//instantiate a button for every host on Master Server
	            GameObject button = (GameObject)Instantiate(buttonPrefab);
	            button.GetComponentInChildren<Text>().text = ResToString(resolutions[i]);
	            int index = i;
	            button.GetComponent<Button>().onClick.AddListener(
	                    () =>
	                    {
	                        SetResolution(index);
	                        clickSource.Play();
	                    });
	            button.transform.SetParent(availableResPanel, false);
			}
			if (resCounter >= 12) {
				scrollBar.SetActive(true);
			}
        }
    }

    void SetResolution(int index)
    {
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, isFullScreen);
    }

	public void SetWindowedRes()
	{
		if (isFullScreen) {
						isFullScreen = false;
						fullWindow.text = "Full Screen";
				} else {
						isFullScreen = true;
						fullWindow.text = "Window";
				}
		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.width, isFullScreen);
	}

    string ResToString(Resolution res)
    {
        return res.width + " x " + res.height;
    }
}
