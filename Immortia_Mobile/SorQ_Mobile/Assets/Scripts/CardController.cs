﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CardController : MonoBehaviour {

    private Transform tr;

    GameManager gameManager;
    AttackManager attackManager;
    EffectManager effectManager;
    InfoManager infoManager;

	public int cardIndex;
    public int posInHand;
    public int clickCount = 0;
    private int cardPwr;
	public Texture[] cardFaces;
	public Texture cardBack;
	public Material cardBackMaterial;
    public Text nameText;
    public Text descText;

    public NetworkViewID viewID;
    
	private static float xVector = 3.2f;
	private static float yVector = 0.05f;
	private static float zVector = 4.8f;
    private static float inHandZoomOffset = 15.0f;

	public Vector3 scaleVector = new Vector3(xVector, yVector, zVector);
    public Vector3 selectScaleVector = new Vector3(xVector / 2, yVector, zVector / 2);
    public float cardSelectScaleMultiplier = 1.15f;
	public Vector3 mousePos;
    private Vector3 mousePosFlipped;
	public Vector3 origScale;
    public Vector3 origPos;
    public Vector3 origPosForEnemy;
    private Vector3 cardZoomPos;


	public bool cardZoomed = false;
	public bool isDragging = false;
    public bool cardEquip = false;
	public bool snapCard;
	public bool cardSnapped;
    public bool cardInPlay;
    public bool cardSelected;
    public bool cardInHand;
    public bool hasAttacked;

	// Use this for initialization
	void Start () {
        attackManager = GameObject.Find("AttackManager").GetComponent<AttackManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	    effectManager = GameObject.Find("EffectManager").GetComponent<EffectManager>();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();

		tr = gameObject.transform;
		//origScale = tr.localScale;
        origScale = gameObject.transform.localScale;
        cardInHand = true;

        viewID = Network.AllocateViewID();

        cardPwr = gameObject.GetComponent<Card>().cardPwr;
        hasAttacked = false;

        findComponents();

	}
	
	// Update is called once per frame
	void Update () {

		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 0.0f;
        mousePosFlipped = mousePos;
        cardZoomPos = gameObject.transform.position;

		if (isDragging) { 
			mousePos.z = -0.5f;
			gameObject.transform.position = mousePos;

		}

        if (cardInHand)
        {
            findPosInHand();
        }
		
	}

	//Called when the mouse enters the card object.
	void OnMouseEnter()
	{
		Debug.Log ("Mouse entered card.");
	}

	//Called when the mouse exits the card object.
	void OnMouseExit()
	{
		Debug.Log ("Mouse exited card.");
        cardSelected = false;
        //gameManager.masterCardSelect = false;

		//If the card is currently zoomed in when the mouse leaves the area of the zoomed card, the card is zoomed back out to its original size/position.
		if (cardZoomed) {
			zoomOutCard ();
		}
	}


    //Called when the mouse left-click is pressed.
    //Records the original position of the card for use later (origPos).

	void OnMouseDown()
	{

        origPos = gameObject.transform.position;
        Debug.Log("Within OnMouseDown()");
       
        //If the card is in play (in the play zone), and its tag is "Player",
        //and the game manager's master select is false, and the card has
        //not attacked, and the card is not dazed,
        //then cardSelected is toggled and the game manager's master select
        //is set to true.

        if (cardInPlay && gameObject.tag == "Player" && gameManager.isPlayerTurn && hasAttacked == false && !gameObject.GetComponent<Card>().isDazed && !gameManager.gameOver && !gameManager.selectCard) 
        {

            if (clickCount == 0)
            {
                cardSelected = true;
                clickCount++;
            }

            else if (clickCount == 1)
            {
                cardSelected = false;
                clickCount = 0;
            }
            
            
            if (cardSelected)
            {
                Debug.Log("Card selected.");
                gameObject.transform.localScale *= cardSelectScaleMultiplier;
                attackManager.attackCardStr = gameObject.GetComponent<Card>().cardStr;
                attackManager.attackCardVit = gameObject.GetComponent<Card>().cardVit;
                attackManager.attackID = gameObject.networkView.viewID;
            }
            
            else if (!cardSelected) {
                Debug.Log("Card unselected.");
                attackManager.readyToAttack = false;
                gameObject.transform.localScale = origScale;
            }

        }

        else if (cardInPlay && gameObject.tag == "Player" && gameManager.isPlayerTurn && hasAttacked == true)
        {
            Debug.Log("Card selected, but card has already attacked.");
            gameManager.showError(5);
        }



        if (cardInPlay && gameObject.tag == "Opponent" && gameManager.isPlayerTurn && gameManager.enemyShieldWall == false && !NetworkView.Find(attackManager.attackID).GetComponent<CardController>().hasAttacked)
        {
            attackManager.defendCardStr = gameObject.GetComponent<Card>().cardStr;
            attackManager.defendCardVit = gameObject.GetComponent<Card>().cardVit;
            attackManager.defendID = gameObject.networkView.viewID;

            Vector3 defendCardPos = NetworkView.Find(attackManager.defendID).gameObject.transform.position;
            defendCardPos = new Vector3(defendCardPos.x, defendCardPos.y - 50, defendCardPos.z - 0.1f);

            Debug.Log("Attacked card with " + attackManager.defendID.ToString() + " networkView ID.");
            NetworkView.Find(attackManager.attackID).gameObject.transform.localScale = origScale;
            StartCoroutine(gameManager.attackCardMove(attackManager.attackID, NetworkView.Find(attackManager.attackID).gameObject.transform.position, defendCardPos, 0.1f));
            //gameManager.networkView.RPC("attackCardMoveForEnemy", RPCMode.Others, NetworkView.Find(attackManager.attackID).gameObject.networkView.viewID);
            //attackManager.attackCard(attackManager.attackID, attackManager.defendID, attackManager.attackCardStr, attackManager.attackCardVit, attackManager.defendCardStr, attackManager.defendCardVit);
            gameManager.masterCardSelect = false;

        }

        else if (cardInPlay && gameObject.tag == "Opponent" && gameManager.isPlayerTurn && gameManager.enemyShieldWall == true)
        {

            attackManager.defendCardStr = gameObject.GetComponent<Card>().cardStr;
            attackManager.defendCardVit = gameObject.GetComponent<Card>().cardVit;
            attackManager.defendID = gameObject.networkView.viewID;


            if (gameObject.GetComponent<Card>().hasShieldWall == true && !NetworkView.Find(attackManager.attackID).GetComponent<CardController>().hasAttacked)
            {
				Vector3 defendCardPos = NetworkView.Find(attackManager.defendID).gameObject.transform.position;
				defendCardPos = new Vector3(defendCardPos.x, defendCardPos.y - 50, defendCardPos.z - 0.1f);

                NetworkView.Find(attackManager.attackID).gameObject.transform.localScale = origScale;
                //attackManager.attackCard(attackManager.attackID, attackManager.defendID, attackManager.attackCardStr, attackManager.attackCardVit, attackManager.defendCardStr, attackManager.defendCardVit);
				StartCoroutine(gameManager.attackCardMove(attackManager.attackID, NetworkView.Find(attackManager.attackID).gameObject.transform.position, defendCardPos, 0.1f));
                //gameManager.networkView.RPC("attackCardMoveForEnemy", RPCMode.Others, attackManager.attackID);
                gameManager.masterCardSelect = false;                
            }
            else
            {
				NetworkView.Find (attackManager.attackID).gameObject.transform.localScale = origScale;
                gameManager.showError(6);
                gameManager.masterCardSelect = false;
            }
        }

        if (cardInPlay && gameObject.tag == "Player" && gameManager.isPlayerTurn && hasAttacked)
        {
            gameManager.showError(5);
        }

        if (cardInPlay && gameObject.tag == "Player" && gameManager.isPlayerTurn && hasAttacked == false && gameObject.GetComponent<Card>().isDazed)
        {
            gameManager.showError(7);
        }

        

		//Checking to see if the card is being left-clicked.
		if (Input.GetMouseButtonDown (0) && !cardSnapped && gameObject.tag == "Player") {
			Debug.Log ("Mouse left-clicked while in card. OnMouseDown()");
            
          
			//If the card is zoomed in when the mouse is left-clicked, the card is zoomed out.
			if (cardZoomed) {
				zoomOutCard ();
			}

			isDragging = true;
		}

	}

	//Called when the mouse is de-pressed.
	//Sets the isDragging boolean value to false.

	void OnMouseUp()
	{
		isDragging = false;

        //If snapCard is true, then the card's position is changed to the snapPos, or the position of the collider.
        //The RPC playCardForEnemy is invoked, cardSnapped and cardInPlay are set to true, and snapCard is set to false.

        //If snapCard is true (snapCard is true when a card has crossed into the collider of the play zone),
        //then the game manager checks that a play space is available. If a play space is available,
        //then the card is played into the first available play space. Along with this, the play is sent across
        //the network to the other client and handled appropriately for them.

        //If snapCard is false, then the card is placed back to its original hand position.
		if (snapCard) {


            //LOGIC FOR MINION CARDS VVVVVVVVVVV
            if (!gameObject.GetComponent<Card>().isSpell)
            {
                if (gameManager.gameBoard.isPlaySpaceAvailable() && gameManager.isPlayerTurn && gameManager.canPlayCard(gameObject, cardPwr) && !gameObject.GetComponent<Card>().isSpell)
                {
                    Debug.Log("Play Space Available");
                    playCard(viewID);

                    // This code was moved in from GameManager, as it seemed to make more sense to have it here.
                    gameManager.player.playerCurrentPower -= gameObject.GetComponent<Card>().cardPwr;
                    GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = gameManager.player.playerCurrentPower.ToString();
                    gameManager.networkView.RPC("updatePowerForEnemy", RPCMode.Others, gameManager.player.playerCurrentPower, gameManager.player.playerMaxPower);

                    gameManager.fieldUpdated = true;
                    networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
                }

                else if (!gameManager.gameBoard.isPlaySpaceAvailable() && gameManager.isPlayerTurn)
                {
                    gameObject.transform.position = origPos;
                    gameManager.showError(1);
                }

                else if (!gameManager.isPlayerTurn)
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
                    gameManager.showError(3);
                }

                else if (gameManager.gameBoard.isPlaySpaceAvailable() && gameManager.isPlayerTurn && !gameManager.canPlayCard(gameObject, cardPwr))
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
                    gameManager.showError(4);
                }
            }

            //LOGIC FOR SPELL CARDS VVVVVVVVVVVVVV
            else if (gameObject.GetComponent<Card>().isSpell)
            {
                if (gameManager.gameBoard.isSpellSpaceAvailable() && gameManager.isPlayerTurn && gameManager.canPlayCard(gameObject, cardPwr) && gameObject.GetComponent<Card>().isSpell)
                {
					if (gameObject.GetComponent<Card>().cardType != Card.cardTypes.spellEqp)
					{
	                    Debug.Log("Playing spell card.");
	                    playCard(viewID);

	                    gameManager.player.playerCurrentPower -= gameObject.GetComponent<Card>().cardPwr;
	                    GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = gameManager.player.playerCurrentPower.ToString();
	                    gameManager.networkView.RPC("updatePowerForEnemy", RPCMode.Others, gameManager.player.playerCurrentPower, gameManager.player.playerMaxPower);

	                    gameManager.fieldUpdated = true;
	                    //networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
					}

					else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellEqp)
					{
						Debug.Log("Playing spell card.");
						playCard(viewID);
						
						gameManager.player.playerCurrentPower -= gameObject.GetComponent<Card>().cardPwr;
						GameObject.Find("playerPowerCurrent").GetComponent<Text>().text = gameManager.player.playerCurrentPower.ToString();
						gameManager.networkView.RPC("updatePowerForEnemy", RPCMode.Others, gameManager.player.playerCurrentPower, gameManager.player.playerMaxPower);
						
						gameManager.fieldUpdated = true;
					}
                }

                else if (!gameManager.gameBoard.isSpellSpaceAvailable() && gameManager.isPlayerTurn)
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
                    //gameObject.transform.position = origPos;
                    Debug.Log("You cannot play anymore spell cards! (FULL)");
                }

                else if (!gameManager.isPlayerTurn)
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
                    //gameObject.transform.position = origPos;
                    gameManager.showError(3);
                }

                else if (gameManager.gameBoard.isSpellSpaceAvailable() && gameManager.isPlayerTurn && !gameManager.canPlayCard(gameObject, cardPwr))
                {
                    StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
                    //gameObject.transform.position = origPos;
                    gameManager.showError(4);
                }
            }
		}

        //If snapCard is false, and cardInPlay is false (which means that the card is currently in the player's hand),
        //then the card is sent back to its original hand position.

        if (!snapCard && !cardInPlay)
        {
            StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, origPos, 0.2f));
            //gameObject.transform.position = origPos;
            Debug.Log("Card not in play space.");
        }

        gameManager.selectCard = false;

	}

	void OnMouseDrag()
	{

	}

	void OnMouseOver()
	{

		//Checking to see if the mouse is right-clicked while a card is being moused over,
        //and that the object's tag is "Player"
		//If clicked, code will check if the card is already zoomed in or not.
		//If zoomed in already, card will be zoomed out to appropriate position.
		//If not zoomed in, card will be zoomed in to a readable size.

        if (Input.GetMouseButtonDown(1) && gameObject.tag == "Player")
        {
            Debug.Log("Mouse right-clicked while in card. OnMouseOver()");
            if (!cardZoomed && !isDragging)
            {

                zoomInCard();

            }

            else if (cardZoomed)
            {

                zoomOutCard();

            }
        }

        //Checks to see if the mouse is right-clicked while a card is being moused over,
        //and that the objects tag is "Opponent" and that the card is in play.
        //If clicked while these are true, then the card will be zoomed if it is not already,
        //and zoomed out if it is already zoomed.

        if (Input.GetMouseButtonDown(1) && gameObject.tag == "Opponent" && cardInPlay)
        {

            if (!cardZoomed)
            {
                zoomInCard();
            }


            else if (cardZoomed)
            {
                zoomOutCard();
            }

        }

		//Checking to see if the mouse is middle-clicked while a card is being moused over.
		if (Input.GetMouseButtonDown (2)) {
			Debug.Log ("Mouse middle-clicked while in card. OnMouseOver()");
		}

        if (Input.GetKeyDown(KeyCode.D) && gameManager.playerName == "PRIMAL")
        {
            gameManager.destroyCard(gameObject.networkView.viewID);
        }

	}
	
	//Called when the card collides with another object's collider.
	void OnCollisionEnter(Collision coll)
	{
		Debug.Log ("Collider entered.");

        if (coll.gameObject.name == "HandZone")
        {
            Debug.Log("HandZone collider entered.");
        }

		if (coll.gameObject.name == "PlayZone" && !cardInPlay) {
            Debug.Log("PlayZone collider entered.");
			snapCard = true;
            
		}

	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player" && coll.gameObject.GetComponent<Card>().cardType == Card.cardTypes.minionCard)
        {
            Debug.Log("Card Collider entered.");
            cardEquip = true;
        }

    }

	//Called when the card leaves another object's collider.
	void OnCollisionExit(Collision coll)
	{
        Debug.Log("Collider exited.");

        if (coll.gameObject.name == "HandZone")
        {
            Debug.Log("HandZone collider exited.");
            snapCard = false;
        }

        if (coll.gameObject.name == "PlayZone")
        {
            Debug.Log("PlayZone collider exited.");
            snapCard = false;
        }
		
		
	}

    //Called when a card is right-clicked. Zooms in on the card.
	void zoomInCard()
	{
       
        cardZoomPos.z -= 25.0f;
        gameObject.transform.position = cardZoomPos;
		iTween.ScaleTo (gameObject, scaleVector, 0.5f);     
        
        if (cardInHand)
        {
            Debug.Log(inHandZoomOffset.ToString());
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + inHandZoomOffset, gameObject.transform.position.z);
        }

		cardZoomed = true;
	}

    //Called when the card is right-clicked after being zoomed in or when the mouse leaves the card. Zooms the card out.
	void zoomOutCard()
	{

        cardZoomPos.z += 25.0f;
        gameObject.transform.position = cardZoomPos;
		iTween.ScaleTo (gameObject, origScale, 0.2f);

        if (cardInHand)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - inHandZoomOffset, gameObject.transform.position.z);
        }

        cardZoomed = false;
	}

    public void findPosInHand()
    {
        for (int i = 0; i < gameManager.gameBoard.cardHandPositions.Length; i++)
        {
            if (gameObject.transform.position == gameManager.gameBoard.cardHandPositions[i])
            {
                posInHand = i;
            }
        }
    }

    void findEnemyPosInHand()
    {
        for (int i = 0; i < gameManager.gameBoard.enemyCardHandPositions.Length; i++)
        {
            if (gameObject.transform.position == gameManager.gameBoard.enemyCardHandPositions[i])
            {
                posInHand = i;
            }
        }
    
    }

    void findComponents()
    {
        Text[] textArr = gameObject.GetComponentsInChildren<Text>();

        for (int i = 0; i < textArr.Length; i++)
        {
            if (textArr[i].name == "nameText")
            {
                nameText = textArr[i];
            }

            if (textArr[i].name == "descriptionText")
            {
                descText = textArr[i];
            }

        }
    }


    //Function called locally to play a card with a given NetworkID.
    void playCard(NetworkViewID viewID)
    {
        if (gameObject.GetComponent<Card> ().cardType == Card.cardTypes.minionCard)
        {
			Component[] components;
			Text dazeText;

			findPosInHand ();

			Vector3 playPos = gameManager.gameBoard.calcPlayPosition ();
            StartCoroutine(gameManager.moveCard(gameObject.networkView.viewID, gameObject.transform.position, playPos, 0.2f));
			this.origPos = playPos;

            
			cardSnapped = true;
			cardInPlay = true;
			cardInHand = false;
			snapCard = false;
			gameManager.gameBoard.handPosAvail [posInHand] = true;
			gameManager.gameBoard.handSpaceAvailable++;

			components = gameObject.GetComponentsInChildren<Text> ();

			for (int i = 0; i < components.Length; i++) 
            {
				if (components [i].name == "dazedText" && gameObject.GetComponent<Card> ().isDazed) 
                {
					dazeText = (Text)components [i];
					dazeText.enabled = true;
					Debug.Log ("Daze is visible.");
				}
			}
            infoManager.setPlayerMinions(infoManager.getPlayerMinions() + 1);
		} 

        else if (gameObject.GetComponent<Card> ().cardType == Card.cardTypes.spellNorm)
        {
			findPosInHand ();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
			gameManager.gameBoard.handPosAvail [posInHand] = true;
			gameManager.gameBoard.handSpaceAvailable++;

        } 

        else if (gameObject.GetComponent<Card> ().cardType == Card.cardTypes.spellEqp)
        {

			findPosInHand ();
            
			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
			gameManager.gameBoard.handPosAvail [posInHand] = true;
			gameManager.gameBoard.handSpaceAvailable++;
            

        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellCont)
        {
            GameObject spellIcon;
            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
			findPosInHand ();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
			gameManager.gameBoard.handPosAvail [posInHand] = true;
			gameManager.gameBoard.handSpaceAvailable++;

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.gameObject.tag = "PlayerSpell";
            gameManager.destroyCard(gameObject.networkView.viewID);

            infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);
        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellTrig)
        {
            GameObject spellIcon;
            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
			findPosInHand ();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
			gameManager.gameBoard.handPosAvail [posInHand] = true;
			gameManager.gameBoard.handSpaceAvailable++;

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.gameObject.tag = "PlayerSpell";
            
            gameManager.destroyCard(gameObject.networkView.viewID);

            infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);
        }
    }

    //Remote Procedure Call method that is called when a player moves one of their cards into their play zone.
    //Shows the play being done on the "opponent" side for the opposite client. Requires a card's given Network ID. 
    [RPC]
    void playCardForEnemy(NetworkViewID viewID)
    {
		if (gameObject.GetComponent<Card> ().cardType == Card.cardTypes.minionCard) 
		{
            GameObject obj = NetworkView.Find(viewID).gameObject;
			findEnemyPosInHand ();

			Vector3 playPos = gameManager.gameBoard.calcPlayPositionForEnemy ();
            StartCoroutine(gameManager.moveCard(viewID, obj.transform.position, playPos, 0.2f));
			this.origPosForEnemy = playPos;

			gameObject.transform.Rotate (new Vector3 (0.0f, 180.0f, 0.0f), Space.World);
			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
			gameManager.gameBoard.enemyHandPosAvail [posInHand] = true;

            infoManager.setOpponentMinions(infoManager.getOpponentMinions() + 1);
        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellNorm)
        {
            GameObject spellIcon;
            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPositionForEnemy();

            findEnemyPosInHand();
            gameManager.gameBoard.enemyHandPosAvail[posInHand] = true;
            gameObject.transform.position = new Vector3(0.0f, 0.0f, 15.0f);
			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.gameObject.tag = "OpponentSpell";


            infoManager.setOpponentSpells(infoManager.getOpponentSpells() + 1);

        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellEqp)
        {
            GameObject spellIcon;

            findEnemyPosInHand();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
            gameManager.gameBoard.enemyHandPosAvail[posInHand] = true;
            
            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPositionForEnemy();

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.gameObject.tag = "OpponentSpell";


            infoManager.setOpponentSpells(infoManager.getOpponentSpells() + 1);
        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellTrig)
        {
            GameObject spellIcon;

            findEnemyPosInHand();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
            gameManager.gameBoard.enemyHandPosAvail[posInHand] = true;

            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPositionForEnemy();

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = "Your guess is as good as mine.";
            spellIcon.GetComponent<spellIconController>().spellType = "?????";
            spellIcon.GetComponent<spellIconController>().spellName = "?????";
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.gameObject.tag = "OpponentSpell";

            infoManager.setOpponentSpells(infoManager.getOpponentSpells() + 1);
        }

        else if (gameObject.GetComponent<Card>().cardType == Card.cardTypes.spellCont)
        {
            GameObject spellIcon;

            findEnemyPosInHand();

			this.cardInHand = false;
			this.snapCard = false;
			this.cardInPlay = true;
			this.cardSnapped = true;
            gameManager.gameBoard.enemyHandPosAvail[posInHand] = true;

            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPositionForEnemy();

            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
			spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.gameObject.tag = "OpponentSpell";


            infoManager.setOpponentSpells(infoManager.getOpponentSpells() + 1);
        }
    }



    //Remote Procedure Call method that is called when a card is being dragged. Updates the card's position 
    //on the opposite client, but with a mirrored y-position.
    //In-development.
    [RPC]
    void moveCardForEnemy() 
    {
        gameObject.transform.position = mousePosFlipped;
    }

}
