﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class showCardButtonPanel : MonoBehaviour {

    public GameObject spellInfoPanel;
    public Text spellInfoText;
    public DeckBuilder deckBuilderManager;

    void Start()
    {
        deckBuilderManager = GameObject.Find("Deck Builder Manager").GetComponent<DeckBuilder>();
        spellInfoPanel = deckBuilderManager.spellDescPanel;
    }

	// Use this for initialization
    void OnMouseEnter()
    {
        spellInfoPanel.SetActive(true);
        spellInfoText = GameObject.Find("spellDescText").GetComponent<Text>();
    }

    void OnMouseExit()
    {
        spellInfoPanel.SetActive(false);
        spellInfoText.text = string.Empty;
    }
}
