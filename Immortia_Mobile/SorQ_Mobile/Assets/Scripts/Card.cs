﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {

    public int cardID;
	public string cardName;
	public string cardDesc;
	public string effectString;
    public string type;
	public int cardPwr;
	public int cardStr;
	public int cardVit;
	public int cardEqp;
    public bool isDazed;
    public int dazeDuration;
	public bool hasShieldWall;
	public bool hasPiercing;
	public bool hasQuickWitted;
    public Sprite cardSprite;
    public Sprite typeSprite;
    public string attr;

    // Added for Discard purposes
    public int cardOrigPwr;
    public int cardOrigStr;
    public int cardOrigVit;
    public int cardOrigEqp;

	public int cardStrInc;
	public int cardVitInc;
	public int cardEqpInc;
	public int cardHealAmt;
	public bool cardShieldWall;
	public bool cardPiercing;
	public bool cardQuickWitted;
    public bool isSpell;

	public enum cardTypes {minionCard, spellNorm, spellEqp, spellTrig, spellCont};
	public cardTypes cardType;


    public Card(int ID, string name, int pwr, int str, int vit, int eqp, string attr, string desc, bool spell, string cType)  //add another string parameter to this "string type"
    {
		string imageFileString = ID.ToString ();
        //string typeFileString = typeID.ToString();    Add a "typeID" to database to show which type each minion is
        //                                              Example: Light is type 0
        //                                                       Dark is type 1
        //                                                       Normal is type 2
        //                                                       Mystic is type 3
        //                                                       Elemental is type 4
        //this.cardSprite = Resources.Load<Sprite>("hyperion_image2"); // USE cardID TO LOAD CORRECT IMAGE LATER   Resources.Load<Sprite>(cardID);
		this.cardSprite = Resources.Load<Sprite> (imageFileString);
        //this.typeSprite = Resources.Load<Sprite>("01");
		this.cardID = ID;
        this.cardName = name;
        this.cardPwr = pwr;
        this.cardStr = str;
        this.cardVit = vit;
        this.cardEqp = eqp;
        this.cardOrigEqp = eqp;
        this.cardOrigPwr = pwr;
        this.cardOrigStr = str;
        this.cardOrigVit = vit;
        this.isSpell = spell;
        this.cardDesc = desc;
        this.type = cType;
        this.attr = attr;
        switch (attr)
        {
            case "SW":
                this.hasShieldWall = true;
                break;
            case "QW":
                this.hasQuickWitted = true;
                break;
            case "PI":
                this.hasPiercing = true;
                break;
        }
        switch (cType)
        {
            case "SPELL-N":
                cardType = cardTypes.spellNorm;
                break;
            case "SPELL-E":
                cardType = cardTypes.spellEqp;
                break;
            case "SPELL-T":
                cardType = cardTypes.spellTrig;
                break;
            case "SPELL-C":
                cardType = cardTypes.spellCont;
                break;
        }
    }

}
