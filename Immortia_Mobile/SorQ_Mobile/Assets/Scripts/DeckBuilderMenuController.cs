﻿using UnityEngine;
using System.Collections;

public class DeckBuilderMenuController : MonoBehaviour {

	DeckBuilder deckBuilder;

	// Use this for initialization
	void Start () {
		deckBuilder = GameObject.Find ("Deck Builder Manager").GetComponent<DeckBuilder> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    public void loadMenuScene()
    {
        Application.LoadLevel(0);
        deckBuilder.infoManager.playMenuMusic();
		Destroy(GameObject.Find("GameSceneManagers"));        
    }


	public void createNewDeck() {
		deckBuilder.createNewDeck ();

	}

	public void editDeck() {
		deckBuilder.editExistingDeck ();
	}
}
