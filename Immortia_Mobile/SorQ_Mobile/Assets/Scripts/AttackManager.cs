﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AttackManager : MonoBehaviour {

    GameManager gameManager;

    EffectManager effectManager = new EffectManager();
    Component[] compArray = new Component[4];

    internal int attackCardStr;
    internal int attackCardVit;
    internal NetworkViewID attackID;

    internal bool readyToAttack;


    internal int defendCardStr;
    internal int defendCardVit;
    internal NetworkViewID defendID;



	// Use this for initialization
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}

    internal void attackCard(NetworkViewID attackID, NetworkViewID defendID, int attackStr, int attackVit, int defendStr, int defendVit)
    {


        int newAttackVit;
        int newDefendVit;

        newAttackVit = attackVit - defendStr;
        newDefendVit = defendVit - attackStr;


        compArray = NetworkView.Find(defendID).GetComponentsInChildren<Text>();
        updateDefendCardVit(compArray, newDefendVit);
        gameManager.networkView.RPC("updateCardVitForEnemy", RPCMode.All, defendID, newDefendVit, "vitalityText");

        compArray = NetworkView.Find(attackID).GetComponentsInChildren<Text>();
        updateAttackCardVit(compArray, newAttackVit);
        gameManager.networkView.RPC("updateCardVitForEnemy", RPCMode.All, attackID, newAttackVit, "vitalityText");

        NetworkView.Find(attackID).gameObject.GetComponent<CardController>().hasAttacked = true;
        NetworkView.Find(attackID).gameObject.GetComponent<CardController>().cardSelected = false;


        /*if (newDefendVit <= 0)
        {
            Debug.Log("DESTROY CARD");
            gameManager.networkView.RPC("destroyCard", RPCMode.All, defendID);
        }

        if (newAttackVit <= 0)
        {
            Debug.Log("DESTROY CARD");
            gameManager.networkView.RPC("destroyCard", RPCMode.All, attackID);
        }*/

        readyToAttack = false;

    }

    internal void attackPlayer(NetworkViewID attackID, int attackStr, int playerVit)
    {

        playerVit -= attackStr;
        NetworkView.Find(attackID).GetComponent<CardController>().hasAttacked = true;
        if (playerVit < 0)
        {
            playerVit = 0;
        }
        gameManager.networkView.RPC("updateEnemyHealth", RPCMode.Others, playerVit);
        readyToAttack = false;
    }

    internal void updateDefendCardVit(Component[] compAr, int defVit)
    {
        compAr = NetworkView.Find(defendID).GetComponentsInChildren<Text>();
            
        for (int i = 0; i < compArray.Length; i++)
        {
            if (compAr[i].name == "vitalityText")
            {
                compAr[i].GetComponent<Text>().text = defVit.ToString();
                Debug.Log(defVit.ToString());
            }

        }
    }

    internal void updateAttackCardVit(Component[] compAr, int attVit)
    {
        compAr = NetworkView.Find(attackID).GetComponentsInChildren<Text>();

        for (int i = 0; i < compArray.Length; i++)
        {
            if (compAr[i].name == "vitalityText")
            {
                compAr[i].GetComponent<Text>().text = attVit.ToString();
                Debug.Log(attVit.ToString());
            }

        }
    }
}
