﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {
	//host list looks for all games with this game name
	private string hostGameName = "Immortia";
    private string instanceName = "default";
    private string searchLobby;
    private int refreshCounter = 0;
	private bool searching;
    private bool refreshingHostList;
	
    //Host Data array to store all returned hosted games from Master Server
	private HostData[] hostData;
	private HostData gameName;

    public GameManager gameManager;
    public InfoManager infoManager;
    public MenuController menuController;
	public MenuController menuScript;
	public AudioSource clickSource;
	public InputField createdServer;
	public InputField searchServer;
	public InputField setServerPass;
	public InputField guessServerPass;
	public GameObject buttonPrefab;
    public GameObject passwordPanel;
    public GameObject countDownPanel;
    public GameObject availableLobbyScrollBar;
    public GameObject loadingPanel;
    public Transform availableLobbyPanel;
	public Text usernameLabel;
	public Text hostUser;
    public Text guestUser;
    public Text messageText;
    public Text lobbyName;
    public Text hostWinCount;
    public Text hostLossCount;
    public Text guestWinCount;
    public Text guestLossCount;
    public Text countDownTimer;
	public Toggle isPrivate;
	public Toggle autoRefresh;
	public Button enterButton;
    float countDown = 5.0f;

    public Text guestMinionsText;
    public Text guestSpellsText;
    public Text guestKillsText;
    public Text hostMinionsText;
    public Text hostSpellsText;
    public Text hostKillsText;
    public GameObject postGameStatsPanel;
    public GameObject preGameStatsPanel;
	
    void Start()
    {
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        if (infoManager.getPostGame())
        {
            menuController.postGame();
            postGameStatsPanel.SetActive(true);
            preGameStatsPanel.SetActive(false);
            loadPostGameStats(infoManager.getDisplayName(), infoManager.getPlayerMinions(), infoManager.getPlayerSpells(), lobbyName.text);
            infoManager.setPostGame(false);
        }
        else
        {
            preGameStatsPanel.SetActive(true);
            postGameStatsPanel.SetActive(false);
        }
    }

    void Update()
    {
        //This code block allows the user to be shown the number of hosts as fast as their internet
        //connection will allow them to pull the data from the master server.
        if (refreshingHostList && this.autoRefresh.isOn)
        {
            if (refreshCounter == 30)
            {
                hostData = MasterServer.PollHostList();

                if (!Network.isServer && !Network.isClient)
                {
                    RefreshHostList();
                }
                this.refreshCounter = 0;
            }
            this.refreshCounter++;
        }
        if (getPlayerReady() && getOpponentReady())
        {
            countDown -= Time.deltaTime;
            countDownPanel.SetActive(true);
            countDownTimer.text = countDown.ToString("0") + "...";
            if (countDown <= 0)
            {
                gameManager.startGameFromLobby();
            }
        }
    }

	//Starts an instance of a server, and registers that instance with Unity's Master Server.
	public void StartServer()
	{
        string temp;
		//determine if your game is private or public
		//private games are still public but they must be searched for by keyword.
		string gameType;
        if (this.isPrivate.isOn)
        {
            gameType = "Private";
        }
        else { gameType = "Public"; }
		
		//set password for game if set to private, else set to null
        if (gameType == "Private")
        {
            Network.incomingPassword = setServerPass.text;
        }
        else { Network.incomingPassword = ""; }

        //set name of game
        if (createdServer.text != "")
        {
            instanceName = gameType + ": " + createdServer.text;
        }
        else { instanceName = gameType + ": " + infoManager.getDisplayName(); }

		Network.InitializeServer(2, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(this.hostGameName, instanceName, gameType);
		Network.maxConnections = 1;

        temp = instanceName;
        initializeComponents();
		lobbyName.text = temp.Replace (gameType + ": ", string.Empty);

        this.hostUser.text = infoManager.getDisplayName();
        this.hostWinCount.text = infoManager.getWins().ToString();
        this.hostLossCount.text = infoManager.getLosses().ToString();
    }
	
	//Requests a list of all hosts with a certain game name. "Immortia" for example.
	//Names of all available hosts are listed in the form of clickable buttons that
	//take user to the lobby
	void RefreshHostList() 
	{
		//empty current list so as to not relist the same lobbies more than once
		List<GameObject> children = new List<GameObject>();
		foreach (Transform child in availableLobbyPanel) children.Add(child.gameObject);
		if(children != null)
			children.ForEach(child => Destroy(child));
		
		MasterServer.RequestHostList (hostGameName);
        
		if (hostData == null) messageText.text = "Searching for hosts";
		if (hostData != null) {
						for (int i = 0; i < hostData.Length; i++) {					
								if (hostData [i].connectedPlayers < 2) {
                                        if (i > 11)
                                        {
                                            availableLobbyScrollBar.SetActive(true);
                                        }
										if (!searching) {
												//instantiate a button for every host on Master Server
												gameName = hostData [i];
												GameObject button = (GameObject)Instantiate (buttonPrefab);
												button.GetComponentInChildren<Text> ().text = hostData [i].gameName;
												button.GetComponent<Button> ().onClick.AddListener (() =>
												{
														int index = i;
														if (gameName.comment == "Private") {
																passwordPanel.SetActive (true);
																enterButton.onClick.AddListener (() =>
																{
																		Network.Connect (gameName, guessServerPass.text);
																		if (!Network.isClient) {
																				StartCoroutine (ShowMessage ("**Password is incorrect. Try again**", 5.0f));
																		}
																});
														} else {
																Network.Connect (gameName);
																if (!Network.isClient) {
																		StartCoroutine (ShowMessage ("**Lobby is currently full. Try another.**", 5.0f));
																}
														}
														clickSource.Play ();
												});
												button.transform.SetParent (availableLobbyPanel, false);
										} else {
												if (hostData [i].gameName.ToLower ().Contains (searchLobby.ToLower ())) {
														//instantiate a button for every host on Master Server
														gameName = hostData [i];
														GameObject button = (GameObject)Instantiate (buttonPrefab);
														button.GetComponentInChildren<Text> ().text = hostData [i].gameName;
														button.GetComponent<Button> ().onClick.AddListener (() =>
														{
																int index = i;
																if (gameName.comment == "Private") {
																		passwordPanel.SetActive (true);
																		enterButton.onClick.AddListener (() =>
																		{
																				Network.Connect (gameName, guessServerPass.text);
																				if (!Network.isClient) {
																						StartCoroutine (ShowMessage ("**Password is incorrect. Try again**", 5.0f));
																				}
																		});
																} else {
																		Network.Connect (gameName);
																		if (!Network.isClient) {
																				StartCoroutine (ShowMessage ("**Lobby is currently full. Try another.**", 5.0f));
																		}

																}
																clickSource.Play ();
														}); 
														button.transform.SetParent (availableLobbyPanel, false);
												}
										}
								}
						}
				}
        if (availableLobbyPanel.childCount == 0)
        {
            loadingPanel.SetActive(true);
        }
        else { loadingPanel.SetActive(false); }
		print("refreshed!");
	}

    #region connectionsDisconnections
    //move camera to lobby panel and reset play panel components
    void OnConnectedToServer()
    {
        loadPlayerName(infoManager.getDisplayName(), infoManager.getWins(), infoManager.getLosses(), lobbyName.text);
        menuScript.Lobby();
        passwordPanel.SetActive(false);
		Debug.Log ("should set refresh to false");
		refreshingHostList = false;
		Debug.Log ("is refreshing false");
        preGameStatsPanel.SetActive(true);
        postGameStatsPanel.SetActive(false);
    }

    void OnPlayerConnected()
    {
        loadPlayerName(infoManager.getDisplayName(), infoManager.getWins(), infoManager.getLosses(), lobbyName.text);
        preGameStatsPanel.SetActive(true);
        postGameStatsPanel.SetActive(false);
    }

    void OnDisconnectedFromServer()
    {
        print("i got disconnected here!");
        initializeComponents();
        menuScript.Play();
        string message = "**Host closed server**";
        float delay = 5.0f;
        StartCoroutine(ShowMessage(message, delay));
		refreshingHostList = true;
        setOpponentReady(false);
        setPlayerReady(false);
        countDownTimer.text = string.Empty;
        passwordPanel.SetActive(false);
        countDown = 5.0f;
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        print("the other guy disconnected here");
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
        guestUser.text = "WAITING...";
        countDownTimer.text = string.Empty;
        passwordPanel.SetActive(false);
        setOpponentReady(false);
        setPlayerReady(false);
        countDown = 5.0f;
    }
    #endregion

    //initialize components when players leave the play panel
    public void initializeComponents()
    {
        MasterServer.ClearHostList();
		searchServer.text = string.Empty;
		createdServer.text = string.Empty;
		guessServerPass.text = string.Empty;
		setServerPass.text = string.Empty;
        isPrivate.isOn = false;
		searching = false;
		lobbyName.text = string.Empty;
		guestUser.text = "WAITING...";
        guestWinCount.text = string.Empty;
        guestLossCount.text = string.Empty;
        preGameStatsPanel.SetActive(true);
        postGameStatsPanel.SetActive(false);
    }

    IEnumerator ShowMessage(string message, float delay)
    {
        messageText.text = message;
        yield return new WaitForSeconds(delay);
        messageText.text = string.Empty;
    }

	public void setRefresh()
	{
		refreshingHostList = !refreshingHostList;
	}
	
	//searches for lobbies containing what is typed in search field
	public void searchForHost()
	{
		this.autoRefresh.isOn = false;
		this.searching = true;
		this.searchLobby = searchServer.text.Trim();
		if (!this.autoRefresh.isOn)
		{
			hostData = MasterServer.PollHostList();
			
			if (!Network.isServer && !Network.isClient)
			{
				RefreshHostList();
			}
		}	
	}
	
	public void noSearchForHost()
	{
		this.searching = false;
		this.searchServer.text = string.Empty;
		if (!this.autoRefresh.isOn)
		{
			hostData = MasterServer.PollHostList();
			
			if (!Network.isServer && !Network.isClient)
			{
				RefreshHostList();
			}
		}
	}
	
	public void joinRandomInstance()
	{
		if (hostData != null)
		{
			int rnd = Random.Range (0, hostData.Length);
			while(hostData[rnd].comment == "private")
			{
				rnd = Random.Range(0, hostData.Length);
			}
			Network.Connect(hostData[rnd]);
		}
	}

    //disconnect from network when user backs out of lobby
    public void disconnect()
    {
        if (Network.isServer)
        {
            MasterServer.UnregisterHost();
        }
        Network.Disconnect();
    }

    //Method that is called when a server is successfully intialized.
    void OnServerInitialized()
    {
        print("Server Initialized.");
    }

    //Method that is called when any master server events occur.
    void OnMasterServerEvent(MasterServerEvent MasterServEvent)
    {
        if (MasterServEvent == MasterServerEvent.RegistrationSucceeded)
        {
            print("Registered User Server with Master Server.");
        }
    }

    void loadPlayerName(string name, int wins, int losses, string lobby)
    {
        if (Network.isClient)
        {
            guestUser.text = name;
            guestWinCount.text = wins.ToString();
            guestLossCount.text = losses.ToString();
        }
        else
        {
            hostUser.text = name;
            hostWinCount.text = wins.ToString();
            hostLossCount.text = losses.ToString();
        }

        this.networkView.RPC("loadHostName", RPCMode.AllBuffered, name, wins, losses, lobby);
    }

    void loadPostGameStats(string name, int minions, int spells, string lobby)
    {
        if (Network.isServer)
        {
            hostUser.text = name;
            hostMinionsText.text = minions.ToString();
            hostSpellsText.text = spells.ToString();
            hostKillsText.text = infoManager.getPlayerKills().ToString();
            guestKillsText.text = infoManager.getOpponentKills().ToString();
        }
        else
        {
            guestUser.text = name;
            guestMinionsText.text = minions.ToString();
            guestSpellsText.text = spells.ToString();
            guestKillsText.text = infoManager.getPlayerKills().ToString();
            hostKillsText.text = infoManager.getOpponentKills().ToString();
        }
        this.networkView.RPC("loadPostGameRPC", RPCMode.AllBuffered, name, minions, spells, lobby);
    }

    public void playerIsReady()
    {
        setPlayerReady(true);
        networkView.RPC("isOtherReady", RPCMode.Others, true);
    }
    private bool playerReady;
    private bool opponentReady;

    public bool getOpponentReady()
    {
        return this.opponentReady;
    }
    public void setOpponentReady(bool tru)
    {
        this.opponentReady = tru;
    }

    public bool getPlayerReady()
    {
        return playerReady;
    }
    public void setPlayerReady(bool tru)
    {
        playerReady = tru;
    }

    [RPC]
    void isOtherReady(bool ready)
    {
        setOpponentReady(ready);
    }

    [RPC]
    void loadPostGameRPC(string name, int minions, int spells, string lobby)
    {
        if (Network.isServer)
        {
            guestUser.text = name;
            guestMinionsText.text = minions.ToString();
            guestSpellsText.text = spells.ToString();
        }
        else
        {
            hostUser.text = name;
            hostMinionsText.text = minions.ToString();
            hostSpellsText.text = spells.ToString();
        }
    }

	[RPC]
	void loadHostName(string name, int wins, int losses, string lobby)
	{
        if (Network.isClient)
        {
            hostUser.text = name;
            hostWinCount.text = wins.ToString();
            hostLossCount.text = losses.ToString();
			lobbyName.text = lobby;
        }
        else 
        { 
            guestUser.text = name;
            guestWinCount.text = wins.ToString();
            guestLossCount.text = losses.ToString();
            lobbyName.text = lobby;
        }
    }
}