﻿using UnityEngine;
using System.Collections;

public class HealingWaters : Spell, ISpell {

	GameManager gameManager;
    InfoManager infoManager;
	HealingWaters healingWatersSpell;
	private bool spellIsCast = false;
	private const int HEAL_AMOUNT = 2;

	// Use this for initialization
	void Start () 
	{
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
		healingWatersSpell = new HealingWaters ();
		healingWatersSpell.spellName = "Healing Waters";
		healingWatersSpell.spellType = SpellTypes.spellNorm;
		healingWatersSpell.spellHealing = HEAL_AMOUNT;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameObject.GetComponent<CardController> ().cardInPlay && !spellIsCast) 
		{
			gameObject.GetComponent<CardController>().findPosInHand();
			gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;
			castSpell();
		}
	}


	public void castSpell() 
	{
		gameManager.healAllFriendlyMinionsInPlay(healingWatersSpell.spellHealing);

        GameObject spellIcon;
        Vector3 spellTokenPos;
        spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
        spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
        spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
        spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
        spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
        spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
        spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
        spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
        spellIcon.gameObject.tag = "PlayerSpell";

        infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

        gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
        //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

		gameManager.networkView.RPC("destroyCard", RPCMode.All, gameObject.networkView.viewID);
		spellIsCast = true;
	}


}
