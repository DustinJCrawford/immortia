﻿using UnityEngine;
using System.Collections;


public class Insight : Spell, ISpell {

	GameManager gameManager;
    InfoManager infoManager;
    Insight insightSpell;
	private bool spellIsCast = false;
    private const int DRAW_AMOUNT = 2;

	// Use this for initialization
	void Start () 
	{
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager> ();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
        insightSpell = new Insight();
        insightSpell.spellName = "Insight";
        insightSpell.spellType = Spell.SpellTypes.spellNorm;

	}

	void Update() 
	{
		if (gameObject.GetComponent<CardController> ().cardInPlay && !spellIsCast) 
		{
			gameObject.GetComponent<CardController>().findPosInHand();
			gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;
			castSpell();
		}
	}	
	
	public void castSpell()
	{

		if (gameManager.gameBoard.handSpaceAvailable < DRAW_AMOUNT)
		{
			StartCoroutine(gameManager.moveCard (gameObject.networkView.viewID, gameObject.transform.position, gameObject.GetComponent<CardController>().origPos, 0.2f));
			gameManager.showError(8);
            gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = false;
            gameManager.addPlayerPower(gameObject.GetComponent<Card>().cardPwr);
			gameObject.GetComponent<CardController>().cardInPlay = false;
			gameObject.GetComponent<CardController>().cardSnapped = false;
			spellIsCast = false;
		}

		else 
		{
			gameManager.drawCard(DRAW_AMOUNT);

            GameObject spellIcon;
            Vector3 spellTokenPos;
            spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
            spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
            spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
            spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
            spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
            spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
            spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
            spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
            spellIcon.gameObject.tag = "PlayerSpell";

            infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

            gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
            //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

			gameManager.networkView.RPC("destroyCard", RPCMode.All, gameObject.networkView.viewID);
			spellIsCast = true;
		}

	}
}
