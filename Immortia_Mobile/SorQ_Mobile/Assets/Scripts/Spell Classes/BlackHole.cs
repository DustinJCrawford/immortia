﻿using UnityEngine;
using System.Collections;

public class BlackHole : Spell, ISpell {

	GameManager gameManager;
    InfoManager infoManager;
	BlackHole blackHoleSpell;
	private bool spellIsCast = false;
	
	// Use this for initialization
	void Start () 
	{
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
        infoManager = GameObject.Find("InfoManager").GetComponent<InfoManager>();
		blackHoleSpell = new BlackHole ();
		blackHoleSpell.spellName = "Black Hole";
		blackHoleSpell.spellType = SpellTypes.spellNorm;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameObject.GetComponent<CardController> ().cardInPlay && !spellIsCast) 
		{
			gameObject.GetComponent<CardController>().findPosInHand();
			gameManager.gameBoard.handPosAvail[gameObject.GetComponent<CardController>().posInHand] = true;
			castSpell();
		}
	}

	public void castSpell() 
	{
		gameManager.globalCardInPlayDestroy ();

        GameObject spellIcon;
        Vector3 spellTokenPos;
        spellTokenPos = gameManager.gameBoard.calcSpellPlayPosition();
        spellIcon = (GameObject)Instantiate(gameManager.spellZoneIcon, spellTokenPos, Quaternion.identity);
        spellIcon.GetComponent<spellIconController>().spellText = gameObject.GetComponent<Card>().cardDesc;
        spellIcon.GetComponent<spellIconController>().spellType = gameObject.GetComponent<Card>().cardType.ToString();
        spellIcon.GetComponent<spellIconController>().spellName = gameObject.GetComponent<Card>().cardName;
        spellIcon.GetComponent<spellIconController>().cardID = gameObject.GetComponent<Card>().cardID;
        spellIcon.GetComponent<spellIconController>().srcCard = gameObject.GetComponent<Card>();
        spellIcon.GetComponent<spellIconController>().iconType = spellIconController.Type.icon;
        spellIcon.gameObject.tag = "PlayerSpell";

        infoManager.setPlayerSpells(infoManager.getPlayerSpells() + 1);

        gameObject.GetComponent<CardController>().networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);
        //gameManager.networkView.RPC("playCardForEnemy", RPCMode.Others, gameObject.networkView.viewID);

		gameManager.fieldUpdated = true;
		gameManager.endTurn ();
		spellIsCast = true;
	}

}
