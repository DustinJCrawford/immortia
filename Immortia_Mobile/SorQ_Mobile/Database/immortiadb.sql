-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2015 at 06:06 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `immortiadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `ASSIGN_ID` int(16) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `DECK_ID` int(10) unsigned zerofill NOT NULL,
  `CARD_ID` int(3) unsigned zerofill NOT NULL,
  `ASSIGN_TIME` datetime NOT NULL,
  PRIMARY KEY (`ASSIGN_ID`,`DECK_ID`,`CARD_ID`),
  KEY `DECK_ID` (`DECK_ID`),
  KEY `CARD_ID` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `CARD_ID` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `CARD_NAME` varchar(60) NOT NULL,
  `CARD_DESC` longtext NOT NULL,
  `CARD_PWR` int(1) NOT NULL,
  `CARD_STR` int(1) DEFAULT NULL,
  `CARD_VIT` int(1) DEFAULT NULL,
  `CARD_EQP` int(1) DEFAULT NULL,
  `CARD_MC_EFF_ID` int(3) DEFAULT NULL,
  `CARD_SC_EFF_ID` int(3) DEFAULT NULL,
  `CARD_ATRB` set('QW','SW','PI') DEFAULT NULL,
  `CARD_TYPE` set('MINION','SPELL-N','SPELL-C','SPELL-E','SPELL-T') NOT NULL,
  PRIMARY KEY (`CARD_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`CARD_ID`, `CARD_NAME`, `CARD_DESC`, `CARD_PWR`, `CARD_STR`, `CARD_VIT`, `CARD_EQP`, `CARD_MC_EFF_ID`, `CARD_SC_EFF_ID`, `CARD_ATRB`, `CARD_TYPE`) VALUES
(001, 'Holy Recruit', 'A recruit of the light.', 0, 0, 0, 3, NULL, NULL, 'QW', 'MINION'),
(002, 'Salty Squire', 'Oooooooh! He''s so salty!', 0, 0, 1, 3, NULL, NULL, 'SW', 'MINION'),
(003, 'Crouching Nun', 'Silent as the night', 1, 1, 2, 1, NULL, NULL, 'SW', 'MINION'),
(004, 'Field Medic', '', 1, 0, 2, 2, NULL, NULL, NULL, 'MINION'),
(005, 'Cardinal Beardsey', 'He has the biggest and bushiest beard of all the Cardinals.', 1, 1, 2, 0, NULL, NULL, NULL, 'MINION'),
(006, 'Pudgey Pope', 'He''s got quite the pudge on him.', 2, 1, 3, 2, NULL, NULL, NULL, 'MINION'),
(007, 'Hidden Monk', 'Silence is golden.', 2, 2, 2, 1, NULL, NULL, 'SW', 'MINION'),
(008, 'Fledgling Cleric', 'He''s just startin'' out. He''s got a lot to learn.', 2, 2, 2, 2, NULL, NULL, 'SW', 'MINION'),
(009, 'Pudgey Paladin', 'This Paladin may be pudgey, but he packs a profound punch.', 3, 2, 4, 1, NULL, NULL, NULL, 'MINION'),
(010, 'Midget Inquisitor', 'Hard to see, packs a punch.', 3, 2, 3, 0, NULL, NULL, NULL, 'MINION'),
(011, 'Priestess of Promise', '', 3, 1, 2, 1, NULL, NULL, NULL, 'MINION'),
(012, 'Pegasus Knight', 'Atop his mighty Pegasus, none can stop him.', 4, 2, 4, 1, NULL, NULL, 'QW', 'MINION'),
(013, 'Nova Knight', 'Brother of Pegasus Knight, he is blindingly shiny.', 4, 2, 4, 1, NULL, NULL, 'SW', 'MINION'),
(014, 'Holy Crusader', 'He will stop at nothing to ensure the Crusade continues.', 5, 2, 6, 1, NULL, NULL, 'SW', 'MINION'),
(015, 'Hyperion, Lucent King', 'Light rains down from the Heavens, and all who stand before him tremble.', 5, 2, 7, 0, NULL, NULL, 'SW', 'MINION'),
(016, 'Lich', 'Evil and deadly.', 1, 2, 1, 1, NULL, NULL, 'QW', 'MINION'),
(017, 'Banshee', 'Her shrieks can deafen you.', 1, 2, 1, 1, NULL, NULL, 'QW', 'MINION'),
(018, 'Animated Armor', 'This armor is alive, and it''s DEADLY!', 1, 1, 2, 2, NULL, NULL, NULL, 'MINION'),
(019, 'Novice Warlock', '', 2, 2, 2, 2, NULL, NULL, NULL, 'MINION'),
(020, 'Bone Cluster', '', 2, 1, 3, 0, NULL, NULL, 'SW', 'MINION'),
(021, 'Undead Brute', '', 2, 3, 1, 1, NULL, NULL, NULL, 'MINION'),
(022, 'Lyra the Lycan', '', 3, 3, 2, 0, NULL, NULL, 'QW', 'MINION'),
(023, 'Necrophage', 'Gain 1 VIT for every friendly minion that is destroyed.', 3, 2, 2, 1, NULL, NULL, NULL, 'MINION'),
(024, 'Death''s Descendant', '', 3, 3, 3, 0, NULL, NULL, NULL, 'MINION'),
(025, 'Skeletal Dragon', '', 4, 4, 4, 0, NULL, NULL, NULL, 'MINION'),
(026, 'Fallen Paladin', '', 4, 4, 3, 1, NULL, NULL, NULL, 'MINION'),
(027, 'Ascendant Warlock', '', 4, 3, 4, 0, NULL, NULL, NULL, 'MINION'),
(028, 'Keeper of Souls', '', 5, 1, 6, 0, NULL, NULL, NULL, 'MINION'),
(029, 'Unchained Cerberus', '', 5, 4, 4, 1, NULL, NULL, NULL, 'MINION'),
(030, 'Malathor', '', 5, 5, 5, 1, NULL, NULL, 'PI', 'MINION'),
(031, 'Gift of the Saints', 'Give 1 PWR to yourself.', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(032, 'Holy Arrow', 'Deal 2 DMG to enemy champion.', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(033, 'Righteous Axe', 'Adds 2 STR to whom it is equipped.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(034, 'Hallowed Helmet', 'Adds 2 VIT to whom it is equipped.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(035, 'Divine Charity', 'Adds 5 VIT to champion of your choosing.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(036, 'Shield of Light', 'Gives shield wall to whom it is equipped.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(037, 'Retribution', 'Next enemy minion to attack has its damage returned to itself without damaging friendly minion.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-T'),
(038, 'Holy Water', 'Heals 1 VIT to all friendly minions for 3 turns.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-C'),
(039, 'Blessed Strength', 'Gives 2 STR to all friendly minions that lasts for 2 turns.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-C'),
(040, 'Relentless Faith', 'Friendly minion of your choice can attack twice this turn, for one turn only.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(041, 'Blessed Armory', 'Gives 2 VIT to all friendly minions.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(042, 'Divine Intervention', 'If player VIT will reach zero from enemy minion attack, player VIT is set to 1, with the difference being done to the minion that attacked the player.', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-T'),
(043, 'Sword of Justice', 'Gives 1 STR every turn to whom it is equipped.', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(044, 'Fountain of Life', 'Reset all friendly minion''s VIT to their base VIT.', 5, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(045, 'Smite', 'Destroys enemy minion of your choice.', 5, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(046, 'Leech Life', 'Damage one friendly minion for 1 VIT and heal yourself for 2 VIT.', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(047, 'Shadow Bolt', 'Deal 2 damage to opponent.', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(048, 'Demon Blade', 'Adds 2 STR to whom it is equipped.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(049, 'Corruption', 'Deal 1 damage to target minion for 2 turns.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-C'),
(050, 'Sacrificial Pact', 'Discard minion from your hand with 3 or more STR and heal your champion for 3 VIT.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(051, 'Malathor''s Vice', 'Increase target minion STR by 3 and decrease minion VIT by 1.', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E'),
(052, 'Necrotic Empowerment', 'Deal 2 damage to yourself to empower all friendly minions with 2 STR.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(053, 'Dark Demise', 'Destroy enemy minion of your choice, subtract minion''s current VIT from your champion''s HP.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(054, 'Terror', 'Fear all enemies for one turn, keeping them from attacking.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(055, 'Blood Link', 'Link 2 friendly minions. Damage done to one minion is divided between linked minions.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-C'),
(056, 'Mind Scatter', 'When an opponent''s minion attacks, it is dazed for 2 turns.', 3, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-T'),
(057, 'Black Plague', 'Deal 1 damage to all minions and champions.', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(058, 'Devil''s Advocate', 'Switch target minion''s STR and VIT.', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(059, 'Soul Reaper', 'Destroy all damaged minions on the board.', 5, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-N'),
(060, 'Dark Resorption', 'Increase target minion''s STR equal to the number of minions of the same name in the discard.', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'SPELL-E');

-- --------------------------------------------------------

--
-- Table structure for table `deck`
--

CREATE TABLE IF NOT EXISTS `deck` (
  `DECK_ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) unsigned zerofill NOT NULL,
  `DECK_NAME` varchar(32) NOT NULL,
  `DECK_CARD_COUNT` int(2) NOT NULL,
  PRIMARY KEY (`DECK_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `match`
--

CREATE TABLE IF NOT EXISTS `match` (
  `MATCH_ID` int(16) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `MATCH_TIME` datetime NOT NULL,
  `MATCH_USER_ONE` int(10) unsigned zerofill NOT NULL,
  `MATCH_USER_TWO` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`MATCH_ID`),
  KEY `MATCH_USER_ONE` (`MATCH_USER_ONE`,`MATCH_USER_TWO`),
  KEY `MATCH_USER_TWO` (`MATCH_USER_TWO`),
  KEY `MATCH_USER_ONE_2` (`MATCH_USER_ONE`),
  KEY `MATCH_USER_TWO_2` (`MATCH_USER_TWO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `PROFILE_ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) unsigned zerofill NOT NULL,
  `PROFILE_WINS` int(12) NOT NULL,
  `PROFILE_LOSSES` int(12) NOT NULL,
  `PROFILE_NAME` varchar(32) NOT NULL,
  `PROFILE_MONEY` int(8) NOT NULL,
  PRIMARY KEY (`PROFILE_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`PROFILE_ID`, `USER_ID`, `PROFILE_WINS`, `PROFILE_LOSSES`, `PROFILE_NAME`, `PROFILE_MONEY`) VALUES
(0000000001, 0000000001, 0, 0, 'username', 50),
(0000000006, 0000000006, 0, 0, 'username2', 50),
(0000000007, 0000000007, 0, 0, 'username3', 50);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USER_ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `USER_LOGIN_NAME` varchar(32) NOT NULL,
  `USER_PW` varchar(32) NOT NULL,
  `USER_F_NAME` varchar(30) NOT NULL,
  `USER_L_NAME` varchar(30) NOT NULL,
  `USER_EMAIL` varchar(60) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USER_ID`, `USER_LOGIN_NAME`, `USER_PW`, `USER_F_NAME`, `USER_L_NAME`, `USER_EMAIL`) VALUES
(0000000001, 'usertest', 'userpw', '', '', 'user@test.com'),
(0000000006, 'usertest2', 'userpw2', '', '', 'user2@test.com'),
(0000000007, 'usertest3', 'userpw3', '', '', 'user3@test.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignment`
--
ALTER TABLE `assignment`
  ADD CONSTRAINT `assignment_ibfk_2` FOREIGN KEY (`DECK_ID`) REFERENCES `deck` (`DECK_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignment_ibfk_3` FOREIGN KEY (`CARD_ID`) REFERENCES `card` (`CARD_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `deck`
--
ALTER TABLE `deck`
  ADD CONSTRAINT `deck_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `match`
--
ALTER TABLE `match`
  ADD CONSTRAINT `match_ibfk_1` FOREIGN KEY (`MATCH_USER_ONE`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_ibfk_2` FOREIGN KEY (`MATCH_USER_TWO`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
